/*        
       DIY Arduino Robot Arm Smartphone Control  
        by Dejan, www.HowToMechatronics.com  
*/
#include <SoftwareSerial.h>
#include <Servo.h>

#define SERIAL_BAUD (115200)

Servo servo01;  // waist range      : [0, 90, 180] right centered left (inverted % wrist roll)
Servo servo02;  // shoulder range   : [0, 90, 180] down centered up (inverted )
Servo servo03;  // elbow range      : [0, 90, 180] up centered down (makes sens use as reference)
Servo servo04;  // wrist roll range : [0, 90, 180] left centered right (this one makes sense!  use as reference)
Servo servo05;  // wrist range      : [0, 90, 180] up (backwards), centered, down (forwards) (this one makes sense!  use as reference)
Servo servo06;  // pincher range    : [0, 90, 134] open, middle, closed (has a limit below 180)

Servo *servoVec[] = {&servo01,&servo02,&servo03,&servo04,&servo05,&servo06};

const int servo1StartPos =  90,
          servo2StartPos =  90, //140,
          servo3StartPos =  90,
          servo4StartPos =  90,
          servo5StartPos =  90,
          servo6StartPos =  90, //80,
          nbServos       =   6;

struct servoRangeInvertStruct {
  bool invert;
  int lowLimit;
  int highLimit;
};

const servoRangeInvertStruct servoLimSVec[nbServos]  = {{true,0,180},
                                                        {true,0,180},
                                                        {false,0,180},
                                                        {false,0,180},
                                                        {false,0,180},
                                                        {false,0,134}};

SoftwareSerial Bluetooth(3, 4); // Arduino(RX, TX) - HC-05 Bluetooth (TX, RX)

int servo1Pos,  servo2Pos,  servo3Pos,  servo4Pos,  servo5Pos,  servo6Pos; // current position
int servo1PPos, servo2PPos, servo3PPos, servo4PPos, servo5PPos, servo6PPos; // previous position

int *sPosVec[]  = {&servo1Pos,  &servo2Pos,  &servo3Pos,  &servo4Pos,  &servo5Pos,  &servo6Pos},
    *sPPosVec[] = {&servo1PPos, &servo2PPos, &servo3PPos, &servo4PPos, &servo5PPos, &servo6PPos};

String servoNameVec[] = { "s1","s2","s3","s4","s5","s6"};
 
int servo01SP[50], servo02SP[50], servo03SP[50], servo04SP[50], servo05SP[50], servo06SP[50]; // for storing positions/steps
int speedDelay = 20;
int index = 0;
String dataIn = "";

void setup() {
  servo01.attach(5);
  servo02.attach(6);
  servo03.attach(7);
  servo04.attach(8);
  servo05.attach(9);
  servo06.attach(10);
  Serial.begin(115200);
  Bluetooth.begin(SERIAL_BAUD); 
  Bluetooth.setTimeout(5);
  delay(20);
  // Robot arm initial position
  servo1PPos = servo1StartPos;
  servo01.write(servo1PPos);
  Serial.println("s1" + String(servo1PPos));

  servo2PPos = servo2StartPos;
  servo02.write(servo2PPos);
  Serial.println("s2" + String(servo2PPos));

  servo3PPos = servo3StartPos;
  servo03.write(servo3PPos);
  Serial.println("s3" + String(servo3PPos));

  servo4PPos = servo4StartPos;
  servo04.write(servo4PPos);
  Serial.println("s4" + String(servo4PPos));

  servo5PPos = servo5StartPos;
  servo05.write(servo5PPos);
  Serial.println("s5" + String(servo5PPos));

  servo6PPos = servo6StartPos;
  servo06.write(servo6PPos);
  Serial.println("s6" + String(servo6PPos));
}

bool checkStr(String tc){
  static const char okVec[] = {'s','.','0','1','2','3','4','5','6','7','8','9'};
  static const int nbOK = 12;
  bool runningRes = true;
  Serial.println("testing: " + tc);
  Serial.println("length: " + String(tc.length()));
 
  for (int i=0;i<min(5,tc.length())&& runningRes;i++){
    bool curCharOk = false;
    //Serial.println("testing: " + String(tc[i]));
    for (int j=0;j<nbOK&& !curCharOk;j++){
      //Serial.println("testing against: " + String(okVec[j]));
      if (tc[i] == okVec[j]){
        curCharOk = true;
        //Serial.println("char is ok: " + String(tc[i]));
      }
    }
    runningRes &=curCharOk;
  }
  return tc.length() !=0 && runningRes;
}

int getServoId(String name){
  int res = -1;
  for (int i=0;i<nbServos;i++){
    if (name.equals(servoNameVec[i])){
      res = i;
      break;
    }
  }
  return res;
}

int getCorrectedServoPos(int sid,int rVal){
  bool invert = servoLimSVec[sid].invert;
  int lowLim  = servoLimSVec[sid].lowLimit,
      highLim = servoLimSVec[sid].highLimit,
      val     = max(lowLim,min(rVal,highLim));

  val = invert ? highLim - (val - lowLim) : val;
  Serial.println("Corrected postion to: " + String(val));
  return val;
}

void updateServoPos(String dataIn){
  String id   = dataIn.substring(0,2);
  int    rVal = dataIn.substring(2,min(dataIn.length(),5)).toInt();
  
  int servoNId = getServoId(id);
  if (servoNId < 0){  // it's "ss set speed" ignore!"
    return;
  }
  Serial.println("Executing: " + id + " : " + String(rVal));
  Serial.println("Servo Id : " + String(getServoId(id)));

  int pVal = getCorrectedServoPos(servoNId,rVal);

  int *sPosPtr  =  sPosVec[servoNId],
      *sPPosPtr = sPPosVec[servoNId];
  
  *sPosPtr = pVal;
  if (*sPPosPtr > *sPosPtr) {
    for ( int j = *sPPosPtr; j >= *sPosPtr; j--) {   // Run servo down
      servoVec[servoNId]->write(j);
      delay(20);    // defines the speed at which the servo rotates
    }
  }
  // If previous position is smaller then current position
  if (*sPPosPtr < *sPosPtr) {
    for ( int j = *sPPosPtr; j <= *sPosPtr; j++) {   // Run servo up
      servoVec[servoNId]->write(j);
      delay(20);
    }
  }
  Serial.println("updated servo: " + servoNameVec[servoNId]);
  Serial.println("Current pos: " +  String(*sPosPtr));
  Serial.println("Previous pos: " + String(*sPPosPtr));
  *sPPosPtr = *sPosPtr;   // set current position as previous position   
}

void loop() {
  dataIn = String("");
  // Check for incoming data
  if (Serial.available()){
    dataIn = Serial.readString();
    Serial.println(dataIn);
  }
  else if((Bluetooth.available() > 0)) {
    dataIn = Bluetooth.readString();
    Serial.println(dataIn);
  }
  dataIn.trim();
  
  if (dataIn.startsWith("SAVE")) {
    doSave();
  }
  else if (dataIn.startsWith("RESET")) {
    doReset();
  }
  else if (dataIn.startsWith("RUN")) {
    Serial.println("Running recorded moves..");
    runservo();  // Automatic mode - run the saved steps 
  }

  // if we got here, then we must process an incoming data string of the for sXnnn.n where x could be a numer of "s"
  else if (dataIn.length()>2){
    dataIn.toLowerCase();
    if (checkStr(dataIn)){
      Serial.println(dataIn.substring(0,2) + " : " + dataIn.substring(2,min(dataIn.length(),5)).toInt());
      updateServoPos(dataIn);
    }
    else{
      Serial.println("bad data in: " + dataIn);
    } 
  }
}
 
void doReset(){
  Serial.println("Reset...");
  memset(servo01SP, 0, sizeof(servo01SP)); // Clear the array data to 0
  memset(servo02SP, 0, sizeof(servo02SP));
  memset(servo03SP, 0, sizeof(servo03SP));
  memset(servo04SP, 0, sizeof(servo04SP));
  memset(servo05SP, 0, sizeof(servo05SP));
  memset(servo06SP, 0, sizeof(servo06SP));
  index = 0;  // Index to 0
}

void doSave(){
  Serial.println("Saving...");
  servo01SP[index] = servo1PPos;  // save position into the array
  servo02SP[index] = servo2PPos;
  servo03SP[index] = servo3PPos;
  servo04SP[index] = servo4PPos;
  servo05SP[index] = servo5PPos;
  servo06SP[index] = servo6PPos;
  index++;                        // Increase the array index
}

int getSpeedDelay(int newSpeed){
  // new speed is on 0, 180
  static const int lowLim  = 5,
                   highLim = 180;
  int val = max(lowLim,min(newSpeed,highLim));
  return highLim - (val - lowLim);
}

// Automatic mode custom function - run the saved steps
void runservo() {
  Serial.println("Running...");
  while (!dataIn.startsWith("RESET")) {   // Run the steps over and over again until "RESET" button is pressed
    for (int i = 0; i <= index - 2; i++) {  // Run through all steps(index)
      bool doSomething = false;
      if (Serial.available()){
        dataIn = Serial.readString();
        doSomething = true;
      }
      else if (Bluetooth.available() > 0) {      // Check for incomding data
        dataIn = Bluetooth.readString();
        doSomething = true;
      }
      if (doSomething && dataIn.startsWith("PAUSE")) {           // If button "PAUSE" is pressed
          while (!dataIn.startsWith("RUN")) {         // Wait until "RUN" is pressed again
            bool doSomethingElse = false;
            if (Serial.available()){
              dataIn = Serial.readString();
              doSomethingElse = true;
            }
            else if (Bluetooth.available() > 0) {      // Check for incomding data
              dataIn = Bluetooth.readString();
              doSomethingElse = true;
            }        
            if (doSomethingElse && dataIn.startsWith("RESET")) {     
              break;
            }
          }
        }
        // If speed slider is changed
        if (dataIn.startsWith("ss")) {
          speedDelay = getSpeedDelay(dataIn.substring(2,min(dataIn.length(),5)).toInt()); // Change servo speed (delay time)
        }
      Serial.println("Step: " + String(i));
      // Servo 1
      if (servo01SP[i] == servo01SP[i + 1]) {
      }
      if (servo01SP[i] > servo01SP[i + 1]) {
        for ( int j = servo01SP[i]; j >= servo01SP[i + 1]; j--) {
          servo01.write(j);
          delay(speedDelay);
        }
      }
      if (servo01SP[i] < servo01SP[i + 1]) {
        for ( int j = servo01SP[i]; j <= servo01SP[i + 1]; j++) {
          servo01.write(j);
          delay(speedDelay);
        }
      }

      // Servo 2
      if (servo02SP[i] == servo02SP[i + 1]) {
      }
      if (servo02SP[i] > servo02SP[i + 1]) {
        for ( int j = servo02SP[i]; j >= servo02SP[i + 1]; j--) {
          servo02.write(j);
          delay(speedDelay);
        }
      }
      if (servo02SP[i] < servo02SP[i + 1]) {
        for ( int j = servo02SP[i]; j <= servo02SP[i + 1]; j++) {
          servo02.write(j);
          delay(speedDelay);
        }
      }

      // Servo 3
      if (servo03SP[i] == servo03SP[i + 1]) {
      }
      if (servo03SP[i] > servo03SP[i + 1]) {
        for ( int j = servo03SP[i]; j >= servo03SP[i + 1]; j--) {
          servo03.write(j);
          delay(speedDelay);
        }
      }
      if (servo03SP[i] < servo03SP[i + 1]) {
        for ( int j = servo03SP[i]; j <= servo03SP[i + 1]; j++) {
          servo03.write(j);
          delay(speedDelay);
        }
      }

      // Servo 4
      if (servo04SP[i] == servo04SP[i + 1]) {
      }
      if (servo04SP[i] > servo04SP[i + 1]) {
        for ( int j = servo04SP[i]; j >= servo04SP[i + 1]; j--) {
          servo04.write(j);
          delay(speedDelay);
        }
      }
      if (servo04SP[i] < servo04SP[i + 1]) {
        for ( int j = servo04SP[i]; j <= servo04SP[i + 1]; j++) {
          servo04.write(j);
          delay(speedDelay);
        }
      }

      // Servo 5
      if (servo05SP[i] == servo05SP[i + 1]) {
      }
      if (servo05SP[i] > servo05SP[i + 1]) {
        for ( int j = servo05SP[i]; j >= servo05SP[i + 1]; j--) {
          servo05.write(j);
          delay(speedDelay);
        }
      }
      if (servo05SP[i] < servo05SP[i + 1]) {
        for ( int j = servo05SP[i]; j <= servo05SP[i + 1]; j++) {
          servo05.write(j);
          delay(speedDelay);
        }
      }

      // Servo 6
      if (servo06SP[i] == servo06SP[i + 1]) {
      }
      if (servo06SP[i] > servo06SP[i + 1]) {
        for ( int j = servo06SP[i]; j >= servo06SP[i + 1]; j--) {
          servo06.write(j);
          delay(speedDelay);
        }
      }
      if (servo06SP[i] < servo06SP[i + 1]) {
        for ( int j = servo06SP[i]; j <= servo06SP[i + 1]; j++) {
          servo06.write(j);
          delay(speedDelay);
        }
      }
    }
  }
}
