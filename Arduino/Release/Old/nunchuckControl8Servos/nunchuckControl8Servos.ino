#include <Wire.h>
#include <Servo.h>
#include "Nunchuk.h"

/* Working!
 *  This version controls 8 servos.
 *  It makes use of the Z and c buttons to set "mode".
 *  The mode then sets the joystick to control pairs of servos:
 *  mode null = servos 4 & 5
 *  mode c    = servos 2 & 3 
 *  mode Z    = servos 0 & 1
 *  Beyond the joystick, the Nunchuk pitch and roll control servos 6 & 7 respectively.
 *  I have implemented "expo-like" softening of the inputs around zero values to allow
 *  a not so steady hand to keep things still.
 *  Direction iversion is implemented as well!
 *  This all works great!
 *  Since no more than 2 servos move at any time, the total current draw is less than 1A.
 */


/* Servos
 *  0 = taille  : Z + joyX
 *  1 = epaule  : Z + joyY
 *  2 = coude   : c + joyX
 *  3 = poignee : c + joyY
 *  4 = main    :     joyX
 *  5 = pince   :     joyY
 *  6 = aux0    :     pitch
 *  7 = aux1    :     roll
 */
const int nbServos = 8;
const int sPin[] = {5,6,7,8,9,10,11,12};
Servo *servo[nbServos];

const int minI      = 0,
          maxI      = 1,
          neutralI  = 2,
          inverterI = 3;
int sRPVec[][4] = { // min 0, max: 1, neutral: 2,inverter 3 (-1 means invert!)
                   {15,170,90,1},     // 0
                   {15,170,90,-1},     // 1
                   {15,170,90,1},     // 2
                   {15,170,90,-1},     // 3
                   {15,170,90,1},     // 4
                   {15,170,90,-1},     // 5
                   {15,170,90,1},     // 6
                   {15,170,90,-1}};    // 7

int sPos[nbServos]; 

float reductionFactor = 0.1;

void initServos(){
  for (int i=0;i<nbServos;i++){
    sPos[i] = sRPVec[i][neutralI];
    servo[i] = new Servo();
    servo[i]->attach(sPin[i]);
    servo[i]->write(sPos[i]);
  }
}

void setup() {
    Serial.begin(115200);
    Wire.begin();
     // Change TWI speed for nuchuk, which uses Fast-TWI (400kHz)
    Wire.setClock(400000);
    nunchuk_init();
    initServos();
}

int _butZ(){
  return nunchuk_buttonZ();
}
int _butC(){
  return nunchuk_buttonC();
} 
int _pitch(){
  return constrain(round(reductionFactor*degrees(nunchuk_pitch())),-10,+10);
}
int _roll(){
  return constrain(round(reductionFactor*degrees(nunchuk_roll())),-10,+10);
}
int _joyX(){ // on [-127,128]
  return constrain(round(0.75*reductionFactor*nunchuk_joystickX()),-10,+10);
}
int _joyY(){ // on [-128,127]
  return constrain(round(0.75*reductionFactor*nunchuk_joystickY()),-10,+10);
}

int softenCenter(int v){
  // v is on [-10,10]
  int av = abs(v),
      sign = (av ? av/v : 1);
      
  if (av <2){
    return 0;
  }
  else if (av<6){
    return (av-2)*sign;
  }
  else {
    return v;
  }
}


int nbFuncs = 6;
typedef int (*npf)();

npf fVec[] = {_butZ,
              _butC,
              _joyX,
              _joyY,
              _roll,
              _pitch};

void priVec(int v1[], int l1, int v2[],int l2){
  for (int i = 0; i< l1;i++){
      Serial.print(v1[i],DEC);
      Serial.print(", ");
  }
  Serial.print("  **  ");
  for (int i = 0; i< l2;i++){
      Serial.print(v2[i],DEC);
      Serial.print(", ");
  }
  Serial.println();
}
void priVec(int v[],int nbElts){
  for (int i=0; i< nbElts; i++){
    Serial.print(v[i],DEC);
    Serial.print(" ");
  }
  Serial.println();
}

void updateServos(int curValVec[]){
  for (int i=0;i<2;i++){
    int j = i;
    if (i ==0){
      j = curValVec[0] ? 0 : curValVec[1] ? 2 : 4;
    }
    else if (i==1){
      j = curValVec[0] ? 1 : curValVec[1] ? 3 : 5;
    }
    sPos[j] = constrain(sPos[j]+sRPVec[j][inverterI]*curValVec[i+2],
                        sRPVec[j][minI],
                        sRPVec[j][maxI]);  //15,170);
      servo[j]->write(sPos[j]);
  }
  for (int i = 6; i<8;i++){
    sPos[i] = constrain(sPos[i]+sRPVec[i][inverterI]*curValVec[i-2],
                        sRPVec[i][minI],
                        sRPVec[i][maxI]);  //15,170);
    servo[i]->write(sPos[i]);
  }
}

void loop() {
  int curValVec[6];
  if (nunchuk_read()) {
    for (int i = 0; i< nbFuncs;i++){
      curValVec[i] = (i>1 ? softenCenter((*fVec[i])()) : (*fVec[i])());
    }
  }
  //nunchuk_print();
  updateServos(curValVec);
  priVec(curValVec,6,sPos,8);
  delay(50);
}
