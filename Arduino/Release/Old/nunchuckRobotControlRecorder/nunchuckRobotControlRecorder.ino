#include <Wire.h>
#include <Servo.h>
#include "Nunchuk.h"

/* Working with the robot arm 2021 03 29
 *  This version can control up to 8 servos.
 *  It makes use of the Z and c buttons to set "mode".
 *  The mode then sets the joystick to control pairs of servos:
 *  mode null = servos 4 & 5
 *  mode c    = servos 2 & 3 
 *  mode Z    = servos 0 & 1
 *  Beyond the joystick, the Nunchuk pitch and roll control servos 6 & 7 respectively.
 *  I have implemented "expo-like" softening of the inputs around zero values to allow
 *  a not so steady hand to keep things still.
 *  Direction iversion is implemented as well!
 *  This all works great!
 *  Since no more than 2 servos move at any time, the total current draw is less than 1A.
 */


/* Servos
 *  0 = taille  : Z + joyX
 *  1 = epaule  : Z + joyY
 *  2 = coude   : c + joyX
 *  3 = poignee : c + joyY
 *  4 = main    :     joyX
 *  5 = pince   :     joyY
 *  6 = aux0    :     pitch // unused
 *  7 = aux1    :     roll  // unused
 */


const unsigned long autoStepDelay    = 20,
                    nunchukLoopDelay = 50;

const int recordPin = 2,
          playPin   = 12;
volatile int  recordPlayState = 0;
 
bool isPlaying = false;
unsigned long lastRecordTime = 0,
              recordPause    = 500;


const int resetPin = 3;  // must be an interrupt pin! (UNO = 2 or 3) !!
volatile int resetState = 0;
unsigned long lastResetTime = 0,
              resetPause    = 1000;

const int nbServos = 6;
const int sPin[] = {5,6,8,7,10,9};  //12,13
Servo *servo[nbServos];

const int minI      = 0,
          maxI      = 1,
          neutralI  = 2,
          inverterI = 3;

/* for test bed only
int sRPVec[][4] = { // min 0, max: 1, neutral: 2,inverter 3 (-1 means invert!)
                   {20,160,90,1},     // 0 waist
                   {20,160,90,1},     // 1 shoulder
                   {20,160,90,1},     // 2 wrist
                   {20,160,90,1},      // 3 elbow
                   {20,160,90,1},     // 4 grip
                   {20,160,90,1}};     // 5 hand
                   //* not used in robot arm
                   {15,170,90,1},     // 6
                   {15,170,90,-1}};    // 7
                   

*/          

int sRPVec[][4] = { // min 0, max: 1, neutral: 2,inverter 3 (-1 means invert!)
                   {0,180,75,-1},     // 0 waist
                   {0,180,95,-1},     // 1 shoulder
                   {0,180,95,-1},     // 2 wrist
                   {0,180,90,1},      // 3 elbow
                   {60,170,90,1},     // 4 grip
                   {0,180,90,1}};     // 5 hand
                   
const String nameVec[] = {"Taille : ", "Epaule : ","Poignee : ", "Coude : ", "Pince : ", "Main : "};
// not used in robot arm
//const String nameVec[] = {"Taille : ", "Epaule : ","Poignee : ", "Coude : ", "Pince : ", "Main : ","Aux1 : ","Aux2 : " } ;

int sPos[nbServos]; 

const float reductionFactor = 0.1;
const int minAngStep = -5,
          maxAngStep = +5;

void setRecPlay(){
  if(digitalRead(recordPin)){
    return;
  }
  if (!digitalRead(playPin)){  // low means pushed, ie play request
    recordPlayState = 2;  // 0 nothing, 1 record requested, 2 play Requested (ie button is low, 3 time out done
  }
  else{
    recordPlayState = 1;
  }
  detachInterrupt(digitalPinToInterrupt(recordPin));
}

typedef struct cell{
  int posVec[nbServos];
  struct cell *next = NULL;
};

struct cell *playlist;


void initPlaylist(){
  playlist = NULL;
}

/*
void initPlaylist(){
  playlist = new cell();
  for (int i=0;i<nbServos;i++){
      playlist->posVec[i] = sRPVec[i][neutralI];
  }
  playlist->next =NULL;
}
*/


void resetPlaylist(){
  while (playlist){
    struct cell *temp = playlist;
    playlist = playlist->next;
    delete temp;
    //Serial.println("deleted one cell");
  }
  initPlaylist();
  isPlaying = false;
  recordPlayState = 3;
  Serial.println("Playlist Reset!");
  printPlaylist();
}

void printPlaylist(){
  Serial.println("Playlist:");
  struct cell *tempPtr = playlist;
  while(tempPtr){
    priVec(tempPtr->posVec,nbServos);
    tempPtr = tempPtr->next;
  }
}


void recordToPlaylist(){
  struct cell **tempPtr = &playlist;
  Serial.println("record to playlist:");  
  while (*tempPtr){
    tempPtr = &((*tempPtr)->next);
    //Serial.print("*tempPtr = ");
    //Serial.println(*tempPtr  == NULL ? "NULL" : "not NULL");
  }
  // now tmpPtr points to the address of last cell in use
  (*tempPtr) = new struct cell();
  (*tempPtr)->next = NULL;
  for (int i=0;i<nbServos;i++){
      (*tempPtr)->posVec[i] = sPos[i];
  }
  Serial.println("Recording positions!");
  priVec(sPos,nbServos);
  printPlaylist();
}

bool doStep(int index, int target){
  // return true if nothing to do
  int delta = target - sPos[index];
  if (!delta){
    return true;
  }
  else if (delta>0){
    sPos[index]++;
  }
  else {
    sPos[index]--;
  }
  servo[index]->write(sPos[index]);
  return false;
}

struct cell *stepCell(cell *curPtr){
  bool moveOn = true;
  if (!curPtr){
    return NULL;
  }
  //Serial.println("stepCell");
  //priVec(curPtr->posVec,nbServos);
  for (int i=0;i<nbServos;i++){
    moveOn = doStep(i,curPtr->posVec[i]) && moveOn;
  }
  delay(autoStepDelay);
  if (moveOn){
    return curPtr->next;
  }
  else{
    return curPtr;
  }
}

bool play(){
  // return true if more to play
  static struct cell *currentCell;
  // check for an abort
  if(!playlist ){
    isPlaying = false;
    currentCell = NULL;
    //Serial.println("currentCell reset!");
  }
  if (isPlaying){
    currentCell = stepCell(currentCell);
  }
  else{
    currentCell = playlist;
  }
  isPlaying = currentCell  != NULL;
  return isPlaying;
}

void initServos(){
  for (int i=0;i<nbServos;i++){
    sPos[i] = sRPVec[i][neutralI];
    servo[i] = new Servo();
    servo[i]->attach(sPin[i]);
    servo[i]->write(sPos[i]);
  }
}

void resetServos(){
  for (int i=0;i<nbServos;i++){
    sPos[i] = sRPVec[i][neutralI];
    servo[i]->write(sPos[i]);
  }
  Serial.println("Servos Reset!");
}

void resetServosAndPlaylist(){
  resetServos();
  resetPlaylist();
}

void setReset(){
  if(digitalRead(resetPin)){
    return;
  }
  resetState = 1;
  detachInterrupt(digitalPinToInterrupt(resetPin));
}
void setup() {
    Serial.begin(115200);
    Wire.begin();
     // Change TWI speed for nuchuk, which uses Fast-TWI (400kHz)
    Wire.setClock(400000);
    nunchuk_init();
    initPlaylist();
    resetPlaylist();
    initServos();
    pinMode(resetPin,  INPUT_PULLUP);
    pinMode(recordPin, INPUT_PULLUP);
    pinMode(playPin,   INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(resetPin), setReset, FALLING);
    attachInterrupt(digitalPinToInterrupt(recordPin), setRecPlay, FALLING);
}

int _butZ(){
  return nunchuk_buttonZ();
}
int _butC(){
  return nunchuk_buttonC();
} 
int _pitch(){
  return constrain(round(reductionFactor*degrees(nunchuk_pitch())),minAngStep,maxAngStep);
}
int _roll(){
  return constrain(round(reductionFactor*degrees(nunchuk_roll())),minAngStep,maxAngStep);
}
int _joyX(){ // on [-127,128]
  return constrain(round(0.75*reductionFactor*nunchuk_joystickX()),minAngStep,maxAngStep);
}
int _joyY(){ // on [-128,127]
  return constrain(round(0.75*reductionFactor*nunchuk_joystickY()),minAngStep,maxAngStep);
}

int softenCenter(int v){
  // v is on [minAngStep,maxAngStep]
  int av = abs(v),
      sign = (av ? av/v : 1);
      
  if (av <2){  // 0 or 1 ->0
    return 0;
  }
  else return v;
}

int nbFuncs = 6;
typedef int (*npf)();

npf fVec[] = {_butZ,
              _butC,
              _joyX,
              _joyY,
              _roll,
              _pitch};

void priVec(int v1[], int l1, int v2[], int l2, String nv[]){
  String outputLine = "";
  char buff[4];
  for (int i = 0; i< l1;i++){
    sprintf(buff,"%3d",v1[i]);
      outputLine += buff; 
      outputLine += (i<l1-1 ? (", ") : "");
  }
  outputLine += ("\t**\t");
  for (int i = 0; i< l2;i++){
    sprintf(buff,"%3d",v2[i]);
    outputLine += nv[i] + buff;
    outputLine += (i<l2-1 ? (", ") : "");
  }
  Serial.println(outputLine);
  }
void priVec(int v1[], int l1, int v2[], int l2){
  String outputLine = "";
  char buff[4];
  for (int i = 0; i< l1;i++){
    sprintf(buff,"%3d",v1[i]);
    outputLine += buff; 
    outputLine += (i<l1-1 ? (", ") : "");
  }
  outputLine += ("\t**\t");
  for (int i = 0; i< l2;i++){
    sprintf(buff,"%3d",v2[i]);
    outputLine += buff;
    outputLine += (i<l2-1 ? (", ") : "");
  }
  Serial.println(outputLine);
}
 
void priVec(int v[],int nbElts){
  for (int i=0; i< nbElts; i++){
    Serial.print(v[i],DEC);
    Serial.print(" ");
  }
  Serial.println();
}

void updateServos(int curValVec[]){
  for (int i=0;i<2;i++){
    int j = i;
    if (i ==0){
      j = curValVec[0] ? 0 : curValVec[1] ? 2 : 4;
    }
    else if (i==1){
      j = curValVec[0] ? 1 : curValVec[1] ? 3 : 5;
    }
    sPos[j] = constrain(sPos[j]+sRPVec[j][inverterI]*curValVec[i+2],
                        sRPVec[j][minI],
                        sRPVec[j][maxI]);  //15,170);
      servo[j]->write(sPos[j]);
  }
  if (nbServos<8){
    return;
  }
  // not used on robot arm with only 6 servos
  for (int i = 6; i<8;i++){
    sPos[i] = constrain(sPos[i]+sRPVec[i][inverterI]*curValVec[i-2],
                        sRPVec[i][minI],
                        sRPVec[i][maxI]);  //15,170);
    servo[i]->write(sPos[i]);
  }
}

bool checkReset(){
  // return true if we do a reset
  switch (resetState){
    case 1: 
      lastResetTime = millis();
      resetServosAndPlaylist();
      resetState = 2;
      return true;
      break;
    case 2:
      if (millis()-lastResetTime>resetPause){
        resetState = 0;
        attachInterrupt(digitalPinToInterrupt(resetPin), setReset, FALLING);
      }
      break;
  }
  return false;
}
bool checkRecordPlay(){
  static int lastState = recordPlayState;
  if(lastState != recordPlayState){
    Serial.println("recordPlayState: " + String(recordPlayState));
    lastState = recordPlayState;
  }
  // return true we want to skip main loop, ie. only if playing
  switch (recordPlayState){
    case 1:  // request to record
      lastRecordTime = millis();
      recordToPlaylist();
      recordPlayState = 3;
      break;
    case 2: // request to play
      if (playlist){
        play();  // this means play again and again
        return true;
      }
      else{
        recordPlayState = 3;  
      }
      //recordPlayState =  play() ? 2 : 3; // keep playing until done, true == more to play
    case 3:
      if (millis()-lastRecordTime>recordPause){
        recordPlayState = 0;
        Serial.println("rearm record interrupt!");
        attachInterrupt(digitalPinToInterrupt(recordPin), setRecPlay, FALLING);
      }
      break;
  }
  return false;
}
bool checkInterruptFlags(){
  return checkReset() || checkRecordPlay();
}
void loop() {  
  if (checkInterruptFlags()){
    return;
  }
  
  int curValVec[6];
  if (nunchuk_read()) {
    for (int i = 0; i< nbFuncs;i++){
      curValVec[i] = (i>1 ? softenCenter((*fVec[i])()) : (*fVec[i])());
    }
  }
  //nunchuk_print();
  updateServos(curValVec);
  //priVec(curValVec,nbFuncs,sPos,nbServos,nameVec); 
  delay(nunchukLoopDelay);
}
