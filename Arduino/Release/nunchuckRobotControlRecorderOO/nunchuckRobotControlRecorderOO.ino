// object oriented version 2021 03 31

/* Working with the robot arm 2021 03 31 
 *  This version can control up to 8 servos.
 *  It makes use of the Z and c buttons to set "mode".
 *  The mode then sets the joystick to control pairs of servos:
 *  mode null = servos 4 & 5
 *  mode c    = servos 2 & 3 
 *  mode Z    = servos 0 & 1
 *  Beyond the joystick, the Nunchuk pitch and roll control 
 *  servos 6 & 7 respectively.
 *  I have implemented "expo-like" softening of the inputs around 
 *  zero values to allow
 *  a not so steady hand to keep things still.
 *  Direction iversion is implemented as well!
 *  This all works great!
 *  Since no more than 2 servos move at any time, the total current draw 
 *  is less than 1A.
 */

#include "App.h"

App *app;

void setup(){
  app = new App();
}

void loop(){
  app->mainLoop();  
}
