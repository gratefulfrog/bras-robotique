#ifndef SERVOMGR_H
#define SERVOMGR_H

#include <Arduino.h>
#include <Servo.h>
#include "Config.h"

class ServoMgr{
  public:
    static const int    nbServos = NB_SERVOS;
    static const String nameVec[nbServos];
  
  protected:
    static const int _sPin[]; 
    static const int _sRPVec[nbServos][4];
    static const int _minI      = 0,
                     _maxI      = 1,
                     _neutralI  = 2,
                     _inverterI = 3;
    Servo *_servoVec[nbServos];
    int _sPos[nbServos];

  public:
    ServoMgr();
    const int * getSPos() const;
    void resetServos();
    void updateServos(int curValVec[]);
    bool stepServo(int index, int target);
};

#endif
