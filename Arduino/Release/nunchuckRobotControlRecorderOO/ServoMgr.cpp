#include "ServoMgr.h"

#ifdef TEST_BED
  const String ServoMgr::nameVec[] = {"Taille : ", 
                                      "Epaule : ",
                                      "Poignee : ", 
                                      "Coude : ", 
                                      "Pince : ", 
                                      "Main : "}; 
                                      //,"Aux1 : ","Aux2 : " } ;
  const int ServoMgr::_sPin[] = {WAIST_PIN,
                                 SHOULDER_PIN,
                                 WRIST_PIN,
                                 ELBOW_PIN,
                                 GRIP_PIN,
                                 HAND_PIN};
                                 //AUX1_PIN,
                                 //AUX2_PIN}; //waist,shoulder,wrist,elbow,grip,hand, aux1,aux2
  const int ServoMgr::_sRPVec[nbServos][4] = { // min 0, max: 1, neutral: 2,inverter 3 (-1 means invert!)
                                               {20,160,90,1},      // 0 waist
                                               {20,160,90,1},      // 1 shoulder
                                               {20,160,90,1},      // 2 wrist
                                               {20,160,90,1},      // 3 elbow
                                               {20,160,90,1},      // 4 grip
                                               {20,160,90,1}};      // 5 hand
                                               //{20,160,90,1},      // 6 aux1
                                               //{20,160,90,1}};     // 7 aux2

#else
  const String ServoMgr::nameVec[] = {"Taille : ", 
                                      "Epaule : ",
                                      "Poignee : ", 
                                      "Coude : ", 
                                      "Pince : ", 
                                      "Main : "};
  const int ServoMgr::_sPin[]  = {WAIST_PIN,
                                  SHOULDER_PIN,
                                  WRIST_PIN,
                                  ELBOW_PIN,
                                  GRIP_PIN,
                                  HAND_PIN}; //waist,shoulder,wrist,elbow,grip,hand,
  const int ServoMgr::_sRPVec[ServoMgr::nbServos][4] =  { // min 0, max: 1, neutral: 2,inverter 3 (-1 means invert!)
                                                         {0,180,75,-1},     // 0 waist
                                                         {0,180,95,-1},     // 1 shoulder
                                                         {0,180,95,-1},     // 2 wrist
                                                         {0,180,90,1},      // 3 elbow
                                                         {60,140,90,1},     // 4 grip
                                                         {0,180,90,1}};     // 5 hand
#endif

ServoMgr::ServoMgr(){
  for (int i=0;i<nbServos;i++){
    _sPos[i] = _sRPVec[i][ServoMgr::_neutralI];
    _servoVec[i] = new Servo();
    _servoVec[i]->attach(_sPin[i]);
    _servoVec[i]->write(_sPos[i]);
  }
}

const int* ServoMgr::getSPos() const{
  return _sPos;
}

void ServoMgr::resetServos(){
  for (int i=0;i<ServoMgr::nbServos;i++){
    _sPos[i] = _sRPVec[i][ServoMgr::_neutralI];
    _servoVec[i]->write(_sPos[i]);
  }
  Serial.println("Servos Reset!");
}

void ServoMgr::updateServos(int curValVec[]){
  for (int i=0;i<2;i++){
    int j = i;
    if (i ==0){
      j = curValVec[0] ? 0 : curValVec[1] ? 2 : 4;
    }
    else if (i==1){
      j = curValVec[0] ? 1 : curValVec[1] ? 3 : 5;
    }
    _sPos[j] = constrain(_sPos[j]+_sRPVec[j][ServoMgr::_inverterI]*curValVec[i+2],
                         _sRPVec[j][ServoMgr::_minI],
                         _sRPVec[j][ServoMgr::_maxI]);  //15,170);
    _servoVec[j]->write(_sPos[j]);
  }
  if (nbServos<8){
    return;
  }
  // not used on robot arm with only 6 servos
  for (int i = 6; i<8;i++){
    _sPos[i] = constrain(_sPos[i]+_sRPVec[i][ServoMgr::_inverterI]*curValVec[i-2],
                         _sRPVec[i][ServoMgr::_minI],
                         _sRPVec[i][ServoMgr::_maxI]);  //15,170);
    _servoVec[i]->write(_sPos[i]);
  }
}

bool ServoMgr::stepServo(int index, int target){
  // return true if nothing to do
  int delta = target - _sPos[index];
  if (!delta){
    return true;
  }
  else if (delta>0){
    _sPos[index]++;
  }
  else {
    _sPos[index]--;
  }
  _servoVec[index]->write(_sPos[index]);
  return false;
}
