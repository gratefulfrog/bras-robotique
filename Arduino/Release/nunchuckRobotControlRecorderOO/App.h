#ifndef APP_H
#define APP_H

#include <Arduino.h>
#include <Wire.h>

#include "Config.h"
#include "Nunchuk.h"

#include "ServoMgr.h"
#include "Automator.h"
#include "tools.h"

class App{
  public:
    // ISR
    static void setReset();
    
  protected:
    static App* _thisPtr;
    const unsigned long _nunchukLoopDelay = NUNCHUK_LOOP_DELAY,
                        _resetPause       = RESET_PAUSE;
    const int     _resetPin      = RESET_PIN;  // must be an interrupt pin! (UNO = 2 or 3) !!
    volatile int  _resetState    = 0;
    unsigned long _lastResetTime = 0;

    Automator *_aut;
    ServoMgr  *_sm;
    
    void _resetServosAndPlaylist();
    bool _checkReset();
    bool _checkInterruptFlags();
    
  public:
    App();
    void mainLoop();
};

#endif
