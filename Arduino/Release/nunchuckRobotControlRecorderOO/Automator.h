#ifndef AUTOMATOR_H
#define AUTOMATOR_H

#include <Arduino.h>
#include "ServoMgr.h"
#include "Config.h"
#include "tools.h"

typedef struct cell{
  int posVec[ServoMgr::nbServos];
  struct cell *next = NULL;
};

class Automator{
  public:
    static void setRecPlay();

  protected:
    static   Automator *_thisPtr;
    volatile int       _recordPlayState = 0;
    static const unsigned long _autoStepDelay = AUTO_STEP_DELAY,
                               _recordPause   = RECORD_PAUSE;
    const int     _recordPin      = RECORD_PIN,  // must be an interrupt pin! (UNO = 2 or 3) !!
                  _playPin        = PLAY_PIN; 
    bool          _isPlaying      = false;
    unsigned long _lastRecordTime = 0;
    struct cell  *_playlist;
    ServoMgr *_smp;

    void _initPlaylist();
    void _printPlaylist() const;
    void _recordToPlaylist();
    struct cell *_stepCell(struct cell *curPtr);
    bool _play();
                
  public:
    Automator(ServoMgr* smp);
    void resetPlaylist();
    bool checkRecordPlay();
    
};

#endif
