#ifndef CONFIG_H
#define CONFIG_H

//#define TEST_BED

#ifdef TEST_BED
#define NB_SERVOS     (6)    
#else
#define NB_SERVOS     (6)    
#endif

//-------------------------- //
// Servo pins
#define AUX1_PIN       (4)
#define WAIST_PIN      (5)
#define SHOULDER_PIN   (6)
#define ELBOW_PIN      (7)
#define WRIST_PIN      (8)
#define HAND_PIN       (9)
#define GRIP_PIN      (10)
#define AUX2_PIN      (11)

// other pins
#define RECORD_PIN     (2)
#define RESET_PIN      (3)
#define PLAY_PIN      (12)

// timing constants
#define AUTO_STEP_DELAY    (20)
#define NUNCHUK_LOOP_DELAY (50)
#define RECORD_PAUSE      (500)
#define RESET_PAUSE      (1000)

// misc constants
#define REDUCTION_FACTOR   (0.1)
#define MIN_ANG_STEP        (-5)
#define MAX_ANG_STEP        (+5)


#endif
