/**
 * @license Nunchuk Arduino library v0.0.1 16/12/2016
 * http://www.xarg.org/2016/12/arduino-nunchuk-library/
 *
 * Copyright (c) 2016, Robert Eisele (robert@xarg.org)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 **/

#ifndef NUNCHUK_H
#define NUNCHUK_H
#include <Arduino.h>
#include <Wire.h>

// Calibration accelerometer values, depends on your Nunchuk
#define NUNCHUK_ACCEL_X_ZERO 509
#define NUNCHUK_ACCEL_Y_ZERO 509
#define NUNCHUK_ACCEL_Z_ZERO 509

// Calibration joystick values
#define NUNCHUK_JOYSTICK_X_ZERO 127
#define NUNCHUK_JOYSTICK_Y_ZERO 128

// Whether to disable encryption. Enabling encryption means that every packet must be decrypted, which wastes cpu cycles. Cheap Nunchuk clones have problems with the encrypted init sequence, so be sure you know what you're doing
#define NUNCHUK_DISABLE_ENCRYPTION

// Print debug information instead of a CSV stream to the serial port
// #define NUNCHUK_DEBUG

// The Nunchuk I2C address
#define NUNCHUK_ADDRESS 0x52

#if ARDUINO >= 100
#define I2C_READ() Wire.read()
#define I2C_WRITE(x) Wire.write(x)
#else
#define I2C_READ() Wire.receive()
#define I2C_WRITE(x) Wire.send(x)
#endif

#define I2C_START(x) Wire.beginTransmission(x)
#define I2C_STOP() Wire.endTransmission(true)

#if defined(__AVR_ATmega168__) || defined(__AVR_ATmega328P__) // Only Arduino UNO
/**
 * Use normal analog ports as power supply, which is useful if you want to have all pins in a row
 * Like for the famous WiiChuck adapter
 * @see https://todbot.com/blog/2008/02/18/wiichuck-wii-nunchuck-adapter-available/
 */
extern void nunchuk_init_power();
#endif
/**
 * Initializes the Nunchuk communication by sending a sequence of bytes
 */
extern void nunchuk_init();

/**
 * Decodes a byte if encryption is used
 *
 * @param x The byte to be decoded
 */
extern inline uint8_t nunchuk_decode_byte(uint8_t x);

/**
 * Central function to read a full chunk of data from Nunchuk
 *
 * @return A boolean if the data transfer was successful
 */
extern uint8_t nunchuk_read() ;
/**
 * Checks the current state of button Z
 */
extern uint8_t nunchuk_buttonZ();

/**
 * Checks the current state of button C
 */
extern uint8_t nunchuk_buttonC() ;

/**
 * Retrieves the raw X-value of the joystick
 */
extern uint8_t nunchuk_joystickX_raw();

/**
 * Retrieves the raw Y-value of the joystick
 */
extern uint8_t nunchuk_joystickY_raw() ;

/**
 * Retrieves the calibrated X-value of the joystick
 */
extern int16_t nunchuk_joystickX() ;

/**
 * Retrieves the calibrated Y-value of the joystick
 */
extern int16_t nunchuk_joystickY() ;

/**
 * Calculates the angle of the joystick
 */
extern float nunchuk_joystick_angle();
/**
 * Retrieves the raw X-value of the accelerometer
 */
extern uint16_t nunchuk_accelX_raw() ;

/**
 * Retrieves the raw Y-value of the accelerometer
 */
extern uint16_t nunchuk_accelY_raw() ;

/**
 * Retrieves the raw Z-value of the accelerometer
 */
extern uint16_t nunchuk_accelZ_raw() ;

/**
 * Retrieves the calibrated X-value of the accelerometer
 */
extern int16_t nunchuk_accelX();
/**
 * Retrieves the calibrated Y-value of the accelerometer
 */
extern int16_t nunchuk_accelY() ;

/**
 * Retrieves the calibrated Z-value of the accelerometer
 */
extern int16_t nunchuk_accelZ() ;

/**
 * Calculates the pitch angle THETA around y-axis of the Nunchuk in radians
 */
extern float nunchuk_pitch() ;

/**
 * Calculates the roll angle PHI around x-axis of the Nunchuk in radians
 */
extern float nunchuk_roll();

/**
 * A handy function to print either verbose information of the Nunchuk or a CSV stream for Processing
 */
extern void nunchuk_print();
#endif
