#include "Automator.h"

Automator* Automator::_thisPtr;

Automator::Automator(ServoMgr* smp): _smp(smp){
  Automator::_thisPtr = this;
  _initPlaylist();
  resetPlaylist();
  pinMode(_recordPin, INPUT_PULLUP);
  pinMode(_playPin,   INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(_recordPin), setRecPlay, FALLING);
}
// static ISR
void Automator::setRecPlay(){
  if(digitalRead(Automator::_thisPtr->_recordPin)){
    return;
  }
  if (!digitalRead(Automator::_thisPtr->_playPin)){  // low means pushed, ie play request
    Automator::_thisPtr->_recordPlayState = 2;  // 0 nothing, 1 record requested, 2 play Requested (ie button is low, 3 time out done
  }
  else{
    Automator::_thisPtr->_recordPlayState = 1;
  }
  detachInterrupt(digitalPinToInterrupt(Automator::_thisPtr->_recordPin));
}

bool Automator::checkRecordPlay(){
  static int lastState = _recordPlayState;
  if(lastState != _recordPlayState){
    //Serial.println("recordPlayState: " + String(_recordPlayState));
    lastState = _recordPlayState;
  }
  // return true we want to skip main loop, ie. only if playing
  switch (_recordPlayState){
    case 1:  // request to record
      _lastRecordTime = millis();
      _recordToPlaylist();
      _recordPlayState = 3;
      break;
    case 2: // request to play
      if (_playlist){
        _play();  // this means play again and again
        return true;
      }
      else{
        _recordPlayState = 3;  
      }
    case 3:
      if (millis()-_lastRecordTime>_recordPause){
        _recordPlayState = 0;
        //Serial.println("rearm record interrupt!");
        attachInterrupt(digitalPinToInterrupt(_recordPin), Automator::setRecPlay, FALLING);
      }
      break;
  }
  return false;
}

void Automator::_initPlaylist(){
  _playlist = NULL;
}

void Automator::resetPlaylist(){
  while (_playlist){
    struct cell *temp = _playlist;
    _playlist = _playlist->next;
    delete temp;
    //Serial.println("deleted one cell");
  }
  _initPlaylist();
  _isPlaying = false;
  _recordPlayState = 3;
  Serial.println("Playlist Reset!");
  _printPlaylist();
}

void Automator::_printPlaylist() const{
  Serial.println("Playlist:");
  struct cell *tempPtr = _playlist;
  while(tempPtr){
    priVec(tempPtr->posVec,ServoMgr::nbServos);
    tempPtr = tempPtr->next;
  }
}

void Automator::_recordToPlaylist(){
  struct cell **tempPtr = &_playlist;
  Serial.println("record to playlist:");  
  while (*tempPtr){
    tempPtr = &((*tempPtr)->next);
    //Serial.print("*tempPtr = ");
    //Serial.println(*tempPtr  == NULL ? "NULL" : "not NULL");
  }
  // now tmpPtr points to the address of last cell in use
  (*tempPtr) = new struct cell();
  (*tempPtr)->next = NULL;
  for (int i=0;i<ServoMgr::nbServos;i++){
      (*tempPtr)->posVec[i] = _smp->getSPos()[i];
  }
  Serial.println("Recording positions!");
  priVec(_smp->getSPos(),ServoMgr::nbServos);
  _printPlaylist();
}

struct cell *Automator::_stepCell(struct cell *curPtr){
  bool moveOn = true;
  if (!curPtr){
    return NULL;
  }
  for (int i=0;i<ServoMgr::nbServos;i++){
    moveOn = _smp->stepServo(i,curPtr->posVec[i]) && moveOn;
  }
  delay(_autoStepDelay);
  if (moveOn){
    Serial.print("Played : ");
    priVec(curPtr->posVec,ServoMgr::nbServos);
    return curPtr->next;
  }
  else{
    return curPtr;
  }
}

bool Automator::_play(){
  // return true if more to play
  static struct cell *currentCell;
  // check for an abort
  if(!_playlist ){
    _isPlaying = false;
    currentCell = NULL;
    //Serial.println("currentCell reset!");
  }
  if (_isPlaying){
    currentCell = _stepCell(currentCell);
  }
  else{
    currentCell = _playlist;
    Serial.println("\nStarting a Play!");
    _printPlaylist();
  }
  _isPlaying = currentCell  != NULL;
  return _isPlaying;
}
