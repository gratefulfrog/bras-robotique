#include "App.h"

void App::_resetServosAndPlaylist(){
  _sm->resetServos();
  _aut->resetPlaylist();
}

App* App::_thisPtr;

void App::setReset(){
  if(digitalRead(App::_thisPtr->_resetPin)){
    return;
  }
  _thisPtr->_resetState = 1;
  detachInterrupt(digitalPinToInterrupt(App::_thisPtr->_resetPin));
}

App::App(){
  Serial.begin(115200);
  
  Wire.begin();
  Wire.setClock(400000); // Change TWI speed for nuchuk, which uses Fast-TWI (400kHz)
  nunchuk_init();

  App::_thisPtr =this;
  _sm =  new ServoMgr();
  _aut = new Automator(_sm);
  pinMode(_resetPin,  INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(_resetPin), App::setReset, FALLING);
}

bool App::_checkReset(){
  // return true if we do a reset
  switch (_resetState){
    case 1: 
      _lastResetTime = millis();
      _resetServosAndPlaylist();
      _resetState = 2;
      return true;
      break;
    case 2:
      if (millis()-_lastResetTime>_resetPause){
        _resetState = 0;
        attachInterrupt(digitalPinToInterrupt(_resetPin), App::setReset, FALLING);
      }
      break;
  }
  return false;
}

bool App::_checkInterruptFlags(){
  return _checkReset() || _aut->checkRecordPlay();
}

void App::mainLoop() {  
  if (_checkInterruptFlags()){
    return;
  }
  
  int curValVec[6];
  if (nunchuk_read()) {
    for (int i = 0; i< nbFuncs;i++){
      curValVec[i] = (i>1 ? softenCenter((*fVec[i])()) : (*fVec[i])());
    }
  }
  _sm->updateServos(curValVec);
  priVec(curValVec,nbFuncs,_sm->getSPos(),ServoMgr::nbServos,ServoMgr::nameVec); 
  delay(_nunchukLoopDelay);
}
