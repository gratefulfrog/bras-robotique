#include "tools.h"

int nbFuncs = 6;

int _butZ(){
  return nunchuk_buttonZ();
}
int _butC(){
  return nunchuk_buttonC();
} 
int _pitch(){
  return constrain(round(REDUCTION_FACTOR*degrees(nunchuk_pitch())),MIN_ANG_STEP,MAX_ANG_STEP);
}
int _roll(){
  return constrain(round(REDUCTION_FACTOR*degrees(nunchuk_roll())),MIN_ANG_STEP,MAX_ANG_STEP);
}
int _joyX(){ // on [-127,128]
  return constrain(round(0.75*REDUCTION_FACTOR*nunchuk_joystickX()),MIN_ANG_STEP,MAX_ANG_STEP);
}
int _joyY(){ // on [-128,127]
  return constrain(round(0.75*REDUCTION_FACTOR*nunchuk_joystickY()),MIN_ANG_STEP,MAX_ANG_STEP);
}

npf fVec[] = {_butZ,
              _butC,
              _joyX,
              _joyY,
              _roll,
              _pitch};

int softenCenter(int v){
  // v is on [minAngStep,maxAngStep]
  int av = abs(v),
      sign = (av ? av/v : 1);
      
  if (av <2){  // 0 or 1 ->0
    return 0;
  }
  else return v;
}


void priVec(const int v1[], int l1, const int v2[], int l2, const String nv[]){
  String outputLine = "";
  char buff[4];
  for (int i = 0; i< l1;i++){
    sprintf(buff,"%3d",v1[i]);
      outputLine += buff; 
      outputLine += (i<l1-1 ? (", ") : "");
  }
  outputLine += ("\t**\t");
  for (int i = 0; i< l2;i++){
    sprintf(buff,"%3d",v2[i]);
    outputLine += nv[i] + buff;
    outputLine += (i<l2-1 ? (", ") : "");
  }
  Serial.println(outputLine);
  }
void priVec(const int v1[], int l1, const int v2[], int l2){
  String outputLine = "";
  char buff[4];
  for (int i = 0; i< l1;i++){
    sprintf(buff,"%3d",v1[i]);
    outputLine += buff; 
    outputLine += (i<l1-1 ? (", ") : "");
  }
  outputLine += ("\t**\t");
  for (int i = 0; i< l2;i++){
    sprintf(buff,"%3d",v2[i]);
    outputLine += buff;
    outputLine += (i<l2-1 ? (", ") : "");
  }
  Serial.println(outputLine);
}
 
void priVec(const int v[],int nbElts){
  for (int i=0; i< nbElts; i++){
    Serial.print(v[i],DEC);
    Serial.print(" ");
  }
  Serial.println();
}
