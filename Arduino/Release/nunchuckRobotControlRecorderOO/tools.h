#ifndef TOOLS_H
#define TOOLS_H

#include <Arduino.h>
#include "Config.h"
#include "Nunchuk.h"

extern int nbFuncs;

typedef int (*npf)();

extern npf fVec[];

extern softenCenter(int v);
extern void priVec(const int v1[], int l1, const int v2[], int l2, const String nv[]);
extern void priVec(const int v1[], int l1, const int v2[], int l2); 
extern void priVec(const int v[],int nbElts);

#endif
