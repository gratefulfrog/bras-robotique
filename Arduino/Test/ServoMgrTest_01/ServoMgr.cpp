#include "ServoMgr.h"
#include <Servo.h>

void ServoMgr::_setPos(int pos){
  _currentPos = pos;
  _servo.write(_currentPos);
}

ServoMgr::ServoMgr(int pin,
                   int nneutralPos,
                   int mminPos,
                   int mmaxPos,
                   int sstepMultiplier):
              neutralPos(nneutralPos),
              minPos(mminPos),
              maxPos(mmaxPos),
              stepMultiplier(sstepMultiplier){  
  _servo.attach(pin);
  _setPos(neutralPos);
  _targetPos = _currentPos;
}
void ServoMgr::setTargetPct(int pct){  // pct is in [0,100]
  //Serial.println(pct);
  int anglePos = round(map(pct,0,100,minPos,maxPos));
  setTargetPos(anglePos);
}  

void ServoMgr::setTargetPos(int pos){
  // pos is angle value
  //_targetPos = max(min(pos,maxPos),minPos);
  _targetPos = pos;
  _sign = ((_targetPos>=_currentPos) ? 1: -1);
  //Serial.println(String("Target set: ") + String(_targetPos));
}

int ServoMgr::getTargetPos() const{
  return _targetPos;
}
int ServoMgr::getCurrentPos() const{
  return _currentPos;
}

bool ServoMgr::update(){
  bool res = true; // return true if we must continue
  int localPos =  _currentPos;
  if (abs(_targetPos-_currentPos) < stepMultiplier){
    localPos = _targetPos;
    res = false;
  }
  else{
    localPos += stepMultiplier*_sign;
  }
  _setPos(localPos); 
  return res;
}
