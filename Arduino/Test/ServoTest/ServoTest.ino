#include <Servo.h>

const int servoPin = 9;
const unsigned long servoStepPause = 0,
                    userPause      = 500;

Servo myservo;  // create servo object to control a servo
// twelve servo objects can be created on most boards

const int startPos       = 90,
          minPos         = 12,
          maxPos         = 170,
          stepMultiplier = 1; 

int curPos    = startPos,
    targetPos = minPos,
    sign      = -1;

void setup() {
  Serial.begin(115200);
  Serial.println("starting up!");
  pinMode(LED_BUILTIN,OUTPUT);
  myservo.attach(servoPin);  // attaches the servo on pin 9 to the servo object
  myservo.write(startPos);  
  delay(userPause);
}

void mywrite(int pos){
  //Serial.print(pos);
  //Serial.print(" ");
  myservo.write(pos);
}

void loop() {
  if (abs(targetPos-curPos) < stepMultiplier){
    curPos = targetPos;
    targetPos = (targetPos == minPos) ? maxPos : minPos;
    sign     *= -1;
    digitalWrite(LED_BUILTIN,!digitalRead(LED_BUILTIN));
    //Serial.println();
    delay(userPause);
  }
  else{
    curPos += stepMultiplier*sign;
  }
  mywrite(curPos);              // tell servo to go to position in variable 'pos'
  delay(servoStepPause);
}
