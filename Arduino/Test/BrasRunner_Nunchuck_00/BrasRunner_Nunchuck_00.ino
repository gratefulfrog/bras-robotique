#include <Wire.h>
#include "Nunchuk.h"

#include "App.h"

/* This version receives incoming servo postions on [0,100] and converts them locally
 *  as a ppropriate
 */

App *app;

/* Servos
 *  0 = taille
 *  1 = epaule
 *  2 = coude
 *  3 = poignee 
 *  4 = main
 *  5 = pince
 */

float reductionFactor = 0.1;

void setup() {
    Serial.begin(115200);
    Wire.begin();
     // Change TWI speed for nuchuk, which uses Fast-TWI (400kHz)
    Wire.setClock(400000);
    // nunchuk_init_power(); // A1 and A2 is power supply
    nunchuk_init();
    app = new App();
}

int _butZ(){
  return nunchuk_buttonZ();
}
int _butC(){
  return nunchuk_buttonC();
}

int _pitch(){
  return constrain(round(reductionFactor*degrees(nunchuk_pitch())),-90,+90);
}

int _roll(){
  return constrain(round(reductionFactor*degrees(nunchuk_roll())),-90,+90);
}
int _joyX(){ // on [-127,128]
  //return round(degrees(sin(nunchuk_joystick_angle())));
  return round(reductionFactor*nunchuk_joystickX());
}
int _joyY(){ // on [-128,127]
  //return round(degrees(cos(nunchuk_joystick_angle())));
  return round(reductionFactor*nunchuk_joystickY());
}

int nbFuncs = 6;
typedef int (*npf)();

npf fVec[] = {_butZ,
              _butC,
              _joyX,
              _joyY,
              _pitch,
              _roll};

void priVec(int v1[], int v2[]){
  for (int i = 0; i< nbFuncs;i++){
      Serial.print(v1[i],DEC);
      Serial.print(", ");
  }
  Serial.print("  **  ");
  for (int i = 0; i< nbFuncs;i++){
      Serial.print(v2[i],DEC);
      Serial.print(", ");
  }
  Serial.print("  **  ");
  for (int i = 0; i< nbFuncs;i++){
      Serial.print((*fVec[i])(),DEC);
      Serial.print(", ");
  }
  Serial.println();
}
void priVec(int v[],int nbElts){
  for (int i=0; i< nbElts; i++){
    Serial.print(v[i],DEC);
    Serial.print(" ");
  }
  Serial.println();
}

void updateVals(int newValVec[]){
  int valVec[] = {0,0,0,0,0,0};

  // coude = button C + pitch
  if (newValVec[1]){
    valVec[2]+= newValVec[4];
  }
  else{
    valVec[1]+= newValVec[4];
  }
  if (newValVec[0]){
    valVec[3]+= newValVec[5];
  }
  else{
    valVec[0]+= newValVec[5];
  }
  valVec[4] += newValVec[2],
  valVec[5] += newValVec[3],

  priVec(valVec,6);
  for (int i=0; i< 6; i++){
    //app->incServoTarget(i, valVec[i]);    
  }
}

void loop() {
  static int minVec[] = {0,0,0,0,0},
             maxVec[] = {0,0,0,0,0};

  int curValVec[6];
    if (nunchuk_read()) {
      for (int i = 0; i< nbFuncs;i++){
        curValVec[i] = (*fVec[i])();
      }
    }
    priVec(curValVec,6);
    updateVals(curValVec);
    app->mainLoop();
    //app->showServoPos();
    delay(50);
} 
