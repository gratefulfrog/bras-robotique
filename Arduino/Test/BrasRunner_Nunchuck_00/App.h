#ifndef APP_H
#define APP_H

#include <Arduino.h>
#include "ServoMgr.h"

class App{
  public:  
    static const int nbServos,
                     servoPinVec[],
                     servoConfVec[][4];
  protected:
    static const char _cycleDoneChar       = 'x',
                      _incomingTerminator  = ';',
                      _incomingSeperator   = ',',
                      _zeroChar            = 'z';
  
    const unsigned long _userPause      = 100,
                        _servoStepDelay = 7; // min value depends on the type of servo and stepMultiplier value
    
    ServoMgr **_smVec;

    void _doZero();
    void _doCommand(String incoming);
    bool _interpretIncoming(String incoming);
    bool _getCommand();
    void _worksDone();


  public:
    App();
    void mainLoop();
    void incServoTarget(int servoId, int inc);
    void showServoPos();
};
#endif
