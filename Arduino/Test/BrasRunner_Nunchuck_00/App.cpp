#include "App.h"

const int App::nbServos       = 6,
          App::servoPinVec[nbServos]  = {5,6,7,8,9,10},
          App::servoConfVec[nbServos][4] = {{90,0,180,1},//{90,180,0,1}, // neutral, min,max,stepMultiplier
                                            {90,0,180,1},//{180-58,180,20,1},
                                            {90,0,180,1},
                                            {96,0,180,1},
                                            {90,0,180,1},
                                            {90,0,180,1}};//{90,0,134,1}};

void App::_doZero(){
  for (int i=0;i<nbServos;i++){
    _smVec[i]->neutralize();
  }
}

void App::incServoTarget(int servoId, int inc){
  _smVec[servoId]->incAnglepos(inc);
}

void App::_doCommand(String incoming){
  // expected message format: 123,456,789,; 
  // where the values can be of any length terminated by ','
  // and the message is terminated with a ';'
  
  int i = 0,
      index  = 0,
      indexOf = incoming.indexOf(_incomingSeperator,index);
  while(indexOf !=-1){
    int pct   = incoming.substring(index,indexOf).toInt();
    _smVec[i++]->setTargetPct(pct);
    index = indexOf+1;
    indexOf = incoming.indexOf(_incomingSeperator,index);
  }
}

bool App::_interpretIncoming(String incoming){
  char c = incoming.charAt(0);
  switch (c){
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      _doCommand(incoming);
      break;
    case _zeroChar:
      _doZero();
      break;
  }
  return true;
}

bool App::_getCommand(){
  if (Serial.available()>0){
    String incoming = Serial.readStringUntil(_incomingTerminator);
    return _interpretIncoming(incoming);
  }
  return false;
}

void App::_worksDone(){
  digitalWrite(LED_BUILTIN,LOW);
  Serial.print(_cycleDoneChar);
}

App::App(){
  Serial.begin(115200);
  //Serial.println("Starting Up!");
  pinMode(LED_BUILTIN,OUTPUT);
  _smVec = new ServoMgr*[nbServos];
  for (int i=0;i<nbServos;i++){
    _smVec[i] = new ServoMgr(servoPinVec[i],
                             servoConfVec[i][0],
                             servoConfVec[i][1],
                             servoConfVec[i][2],
                             servoConfVec[i][3]); 
  }
  delay(_userPause);
}

void App::showServoPos(){
  for (int i=0;i<nbServos;i++){
    Serial.print(_smVec[i]->getCurrentPos());
    Serial.print(" ");
  }
  Serial.println();
}

void App::mainLoop(){
  static bool didWork = false;
  didWork = _getCommand()  || didWork;
  bool cont = false;
  for (int i=0;i<nbServos;i++){
    //cont = _smVec[i]->update() || cont;
  }
  if(cont){
    digitalWrite(LED_BUILTIN,HIGH);
    didWork = true;
    delay(_servoStepDelay);
  }    
  else if (didWork){
    _worksDone();
    didWork = false;
  }
}
