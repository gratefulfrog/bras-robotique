#include "App.h"

const int App::nbServos       = 6,
          App::servoPinVec[nbServos]  = {4,5,6,7,8,9},
          App::servoConfVec[nbServos][4] = {{90,15,170,1}, // neutral, min,max,stepMultiplier
                                            {90,15,170,1},
                                            {90,15,170,1},
                                            {90,15,170,1},
                                            {90,15,170,1},
                                            {90,15,170,1}};

bool App::_interpretIncoming(String incoming){
  // expected message format: 123,456,789,; 
  // where the values can be of any length terminated by ','
  // and the message is terminated with a ';'
  
  int i = 0,
      index  = 0,
      indexOf = incoming.indexOf(_incomingSeperator,index);
  while(indexOf !=-1){
    int pos   = incoming.substring(index,indexOf).toInt();
    _smVec[i++]->setTargetPos(pos);
    index = indexOf+1;
    indexOf = incoming.indexOf(_incomingSeperator,index);
  }
  return true;
}

bool App::_getCommand(){
  if (Serial.available()>0){
    String incoming = Serial.readStringUntil(_incomingTerminator);
    return _interpretIncoming(incoming);
  }
  return false;
}

void App::_worksDone(){
  digitalWrite(LED_BUILTIN,LOW);
  Serial.print(_cycleDoneChar);
}

App::App(){
  Serial.begin(115200);
  //Serial.println("Starting Up!");
  pinMode(LED_BUILTIN,OUTPUT);
  _smVec = new ServoMgr*[nbServos];
  for (int i=0;i<nbServos;i++){
    _smVec[i] = new ServoMgr(servoPinVec[i],
                             servoConfVec[i][0],
                             servoConfVec[i][1],
                             servoConfVec[i][2],
                             servoConfVec[i][3]); 
  }
  delay(_userPause);
}

App::mainLoop(){
  static bool didWork = false;
  didWork = _getCommand()  || didWork;
  bool cont = false;
  for (int i=0;i<nbServos;i++){
    cont = _smVec[i]->update() || cont;
  }
  if(cont){
    digitalWrite(LED_BUILTIN,HIGH);
    didWork = true;
    delay(_servoStepDelay);
  }    
  else if (didWork){
    _worksDone();
    didWork = false;
  }
}
