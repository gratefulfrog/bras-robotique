#ifndef APP_H
#define APP_H

#include <Arduino.h>
#include "ServoMgr.h"

class App{
  public:  
    static const int nbServos,
                     servoPinVec[],
                     servoConfVec[][4];
  protected:
    const char          _cycleDoneChar       = 'x',
                        _incomingTerminator  = ';',
                        _incomingSeperator   = ',',
                        _servoChar           = 's';
    
    const unsigned long _userPause      = 100,
                        _servoStepDelay = 7; // min value depends on the type of servo and stepMultiplier value
    
    ServoMgr **_smVec;

    bool _interpretIncoming(String incoming);
    bool _getCommand();
    void _worksDone();


  public:
    App();
    mainLoop();

};
#endif
