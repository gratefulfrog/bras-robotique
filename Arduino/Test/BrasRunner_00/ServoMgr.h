#ifndef SERVOMGR_H
#define SERVOMGR_H

#include <Arduino.h>
#include <Servo.h>

class ServoMgr{
  protected:
    
    Servo _servo;
    int _targetPos,
        _currentPos,
        _sign;
    void _setPos(int pos);
    
  public:
    const int neutralPos,
              minPos,
              maxPos,
              stepMultiplier = 1; 
    ServoMgr(int pin,
             int neutralPos     = 90,
             int minPos         = 5,
             int maxPos         = 175,
             int stepMultiplier = 1);
    void neutralize();
    void setTargetPct(int pct);  // pct is in [0,100]
    void setTargetPos(int pos);  // pos is angle value
    int getTargetPos() const;
    int getCurrentPos() const;
    bool update();
};

#endif
