EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Bras Robotique"
Date "2020-12-19"
Rev "V 1.0"
Comp "DigiTarn"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Module:Arduino_UNO_R3 A1
U 1 1 5FDDC797
P 4730 3210
F 0 "A1" H 4730 3710 50  0000 C CNN
F 1 "Arduino_UNO_R3" V 4700 3200 50  0000 C CNN
F 2 "Module:Arduino_UNO_R3" H 4730 3210 50  0001 C CIN
F 3 "https://www.arduino.cc/en/Main/arduinoBoardUno" H 4730 3210 50  0001 C CNN
	1    4730 3210
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J1
U 1 1 5FDDF087
P 2330 5570
F 0 "J1" H 2221 5140 50  0000 C CNN
F 1 "Bluetooth Module" H 2221 5233 50  0000 C CNN
F 2 "misc:PinSocket_1x04_P2.54mm_Horizontal_big_pads" H 2330 5570 50  0001 C CNN
F 3 "~" H 2330 5570 50  0001 C CNN
	1    2330 5570
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x03_Male J6
U 1 1 5FDDFD5D
P 2330 3290
F 0 "J6" H 2439 3576 50  0000 C CNN
F 1 "Servo Elbow" H 2439 3483 50  0000 C CNN
F 2 "misc:Pin_Header_Straight_1x03" H 2330 3290 50  0001 C CNN
F 3 "~" H 2330 3290 50  0001 C CNN
	1    2330 3290
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J5
U 1 1 5FDE048E
P 2330 2700
F 0 "J5" H 2439 2986 50  0000 C CNN
F 1 "Servo Shoulder" H 2439 2893 50  0000 C CNN
F 2 "misc:Pin_Header_Straight_1x03" H 2330 2700 50  0001 C CNN
F 3 "~" H 2330 2700 50  0001 C CNN
	1    2330 2700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J7
U 1 1 5FDE3989
P 2370 3840
F 0 "J7" H 2479 4126 50  0000 C CNN
F 1 "Servo Wrist Roll" H 2479 4033 50  0000 C CNN
F 2 "misc:Pin_Header_Straight_1x03" H 2370 3840 50  0001 C CNN
F 3 "~" H 2370 3840 50  0001 C CNN
	1    2370 3840
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J4
U 1 1 5FDE4056
P 2330 4370
F 0 "J4" H 2439 4656 50  0000 C CNN
F 1 "Servo Wrist Pitch" H 2439 4563 50  0000 C CNN
F 2 "misc:Pin_Header_Straight_1x03" H 2330 4370 50  0001 C CNN
F 3 "~" H 2330 4370 50  0001 C CNN
	1    2330 4370
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J2
U 1 1 5FDE442C
P 2330 2030
F 0 "J2" H 2439 2316 50  0000 C CNN
F 1 "Servo Waiste" H 2439 2223 50  0000 C CNN
F 2 "misc:Pin_Header_Straight_1x03" H 2330 2030 50  0001 C CNN
F 3 "~" H 2330 2030 50  0001 C CNN
	1    2330 2030
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J3
U 1 1 5FDE4AC7
P 2330 4920
F 0 "J3" H 2439 5206 50  0000 C CNN
F 1 "Servo Gripper" H 2439 5113 50  0000 C CNN
F 2 "misc:Pin_Header_Straight_1x03" H 2330 4920 50  0001 C CNN
F 3 "~" H 2330 4920 50  0001 C CNN
	1    2330 4920
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Counter_Clockwise J9
U 1 1 5FDF24E7
P 4200 940
F 0 "J9" H 4250 1160 50  0000 C CNN
F 1 "Buck Converter" H 4250 1067 50  0000 C CNN
F 2 "misc:Buck_Converter" H 4200 940 50  0001 C CNN
F 3 "~" H 4200 940 50  0001 C CNN
	1    4200 940 
	1    0    0    -1  
$EndComp
Text Label 2530 1930 0    50   ~ 0
Signal_Waiste
Text Label 2530 2030 0    50   ~ 0
+5v
Text Label 2530 2700 0    50   ~ 0
+5v
Text Label 2530 3290 0    50   ~ 0
+5v
Text Label 2570 3840 0    50   ~ 0
+5v
Text Label 2530 4370 0    50   ~ 0
+5v
Text Label 2530 4920 0    50   ~ 0
+5v
Text Label 2530 5670 0    50   ~ 0
+5v
Text Label 2530 5470 0    50   ~ 0
BT_TX
Text Label 2530 2600 0    50   ~ 0
Signal_Shoulder
Text Label 2530 3190 0    50   ~ 0
Signal_Elbow
Text Label 2570 3740 0    50   ~ 0
Signal_Wrist_Roll
Text Label 2530 4270 0    50   ~ 0
Signal_Wrist_Pitch
Text Label 2530 4820 0    50   ~ 0
Signal_Gripper
Text Label 4500 940  0    50   ~ 0
+5v
Text Label 4230 3110 2    50   ~ 0
Signal_Waiste
Text Label 4230 3210 2    50   ~ 0
Signal_Shoulder
Text Label 4230 3310 2    50   ~ 0
Signal_Elbow
Text Label 4230 3410 2    50   ~ 0
Signal_Wrist_Roll
Text Label 4230 3510 2    50   ~ 0
Signal_Wrist_Pitch
Text Label 4230 3610 2    50   ~ 0
Signal_Gripper
$Comp
L Device:R R2
U 1 1 5FE13AFA
P 3400 5370
F 0 "R2" V 3310 5370 50  0000 C CNN
F 1 "1K" V 3390 5370 50  0000 C CNN
F 2 "misc:Resistor_Horizontal_RM7mm_Big_Pads" V 3330 5370 50  0001 C CNN
F 3 "~" H 3400 5370 50  0001 C CNN
	1    3400 5370
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5FE141A8
P 3250 5520
F 0 "R1" H 3410 5530 50  0000 R CNN
F 1 "2K" V 3250 5570 50  0000 R CNN
F 2 "misc:Resistor_Horizontal_RM7mm_Big_Pads" V 3180 5520 50  0001 C CNN
F 3 "~" H 3250 5520 50  0001 C CNN
	1    3250 5520
	-1   0    0    1   
$EndComp
Text Label 2530 5370 0    50   ~ 0
BT_RX
Text Label 3250 5370 2    50   ~ 0
BT_RX
Text Label 3550 5370 0    50   ~ 0
Arduino_BT_TX
Text Label 4230 3010 2    50   ~ 0
Arduino_BT_TX
Text Label 4230 2910 2    50   ~ 0
BT_TX
NoConn ~ 4230 2610
NoConn ~ 4230 2710
NoConn ~ 4230 2810
NoConn ~ 4830 2210
NoConn ~ 4930 2210
NoConn ~ 5230 2610
NoConn ~ 5230 2810
NoConn ~ 5230 3010
NoConn ~ 5230 3210
NoConn ~ 5230 3310
NoConn ~ 5230 3410
NoConn ~ 5230 3510
NoConn ~ 5230 3610
NoConn ~ 5230 3710
NoConn ~ 5230 3910
NoConn ~ 5230 4010
NoConn ~ 4830 4310
NoConn ~ 4630 4310
$Comp
L power:+12V #PWR0103
U 1 1 5FE366AB
P 4000 940
F 0 "#PWR0103" H 4000 790 50  0001 C CNN
F 1 "+12V" V 4060 890 50  0000 L CNN
F 2 "" H 4000 940 50  0001 C CNN
F 3 "" H 4000 940 50  0001 C CNN
	1    4000 940 
	0    -1   -1   0   
$EndComp
$Comp
L power:+12V #PWR0104
U 1 1 5FE37C93
P 4630 2070
F 0 "#PWR0104" H 4630 1920 50  0001 C CNN
F 1 "+12V" V 4690 2020 50  0000 L CNN
F 2 "" H 4630 2070 50  0001 C CNN
F 3 "" H 4630 2070 50  0001 C CNN
	1    4630 2070
	1    0    0    -1  
$EndComp
NoConn ~ 4230 3710
NoConn ~ 4230 3810
NoConn ~ 4230 3910
$Comp
L power:GND #PWR0102
U 1 1 5FE3A07D
P 4000 1040
F 0 "#PWR0102" H 4000 790 50  0001 C CNN
F 1 "GND" H 4005 864 50  0000 C CNN
F 2 "" H 4000 1040 50  0001 C CNN
F 3 "" H 4000 1040 50  0001 C CNN
	1    4000 1040
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5FE3C7C8
P 4730 4490
F 0 "#PWR0106" H 4730 4240 50  0001 C CNN
F 1 "GND" H 4735 4314 50  0000 C CNN
F 2 "" H 4730 4490 50  0001 C CNN
F 3 "" H 4730 4490 50  0001 C CNN
	1    4730 4490
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5FE3D65D
P 3250 5670
F 0 "#PWR0107" H 3250 5420 50  0001 C CNN
F 1 "GND" H 3255 5494 50  0000 C CNN
F 2 "" H 3250 5670 50  0001 C CNN
F 3 "" H 3250 5670 50  0001 C CNN
	1    3250 5670
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5FE3E41F
P 2530 5570
F 0 "#PWR0108" H 2530 5320 50  0001 C CNN
F 1 "GND" V 2535 5440 50  0000 R CNN
F 2 "" H 2530 5570 50  0001 C CNN
F 3 "" H 2530 5570 50  0001 C CNN
	1    2530 5570
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5FE3F9A4
P 2530 5020
F 0 "#PWR0109" H 2530 4770 50  0001 C CNN
F 1 "GND" V 2535 4890 50  0000 R CNN
F 2 "" H 2530 5020 50  0001 C CNN
F 3 "" H 2530 5020 50  0001 C CNN
	1    2530 5020
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5FE4137D
P 4500 1040
F 0 "#PWR0110" H 4500 790 50  0001 C CNN
F 1 "GND" H 4505 864 50  0000 C CNN
F 2 "" H 4500 1040 50  0001 C CNN
F 3 "" H 4500 1040 50  0001 C CNN
	1    4500 1040
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 5FE4349A
P 2570 3940
F 0 "#PWR0111" H 2570 3690 50  0001 C CNN
F 1 "GND" V 2575 3810 50  0000 R CNN
F 2 "" H 2570 3940 50  0001 C CNN
F 3 "" H 2570 3940 50  0001 C CNN
	1    2570 3940
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5FE43DF8
P 2530 3390
F 0 "#PWR0112" H 2530 3140 50  0001 C CNN
F 1 "GND" V 2535 3260 50  0000 R CNN
F 2 "" H 2530 3390 50  0001 C CNN
F 3 "" H 2530 3390 50  0001 C CNN
	1    2530 3390
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5FE442D8
P 2530 4470
F 0 "#PWR0113" H 2530 4220 50  0001 C CNN
F 1 "GND" V 2535 4340 50  0000 R CNN
F 2 "" H 2530 4470 50  0001 C CNN
F 3 "" H 2530 4470 50  0001 C CNN
	1    2530 4470
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 5FE45483
P 2530 2800
F 0 "#PWR0114" H 2530 2550 50  0001 C CNN
F 1 "GND" V 2535 2670 50  0000 R CNN
F 2 "" H 2530 2800 50  0001 C CNN
F 3 "" H 2530 2800 50  0001 C CNN
	1    2530 2800
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 5FE45DC7
P 2530 2130
F 0 "#PWR0115" H 2530 1880 50  0001 C CNN
F 1 "GND" V 2535 2000 50  0000 R CNN
F 2 "" H 2530 2130 50  0001 C CNN
F 3 "" H 2530 2130 50  0001 C CNN
	1    2530 2130
	0    -1   -1   0   
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5FE489E1
P 4630 2070
F 0 "#FLG0101" H 4630 2145 50  0001 C CNN
F 1 "PWR_FLAG" H 4630 2246 50  0000 C CNN
F 2 "" H 4630 2070 50  0001 C CNN
F 3 "~" H 4630 2070 50  0001 C CNN
	1    4630 2070
	0    -1   -1   0   
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5FE4964D
P 4730 4490
F 0 "#FLG0102" H 4730 4565 50  0001 C CNN
F 1 "PWR_FLAG" H 4730 4666 50  0000 C CNN
F 2 "" H 4730 4490 50  0001 C CNN
F 3 "~" H 4730 4490 50  0001 C CNN
	1    4730 4490
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4730 4310 4730 4490
Connection ~ 4730 4490
Wire Wire Line
	4630 2210 4630 2070
Connection ~ 4630 2070
$EndSCHEMATC
