$fn=100;


prefix = "../";

// part, rotate,translate,nextCenter
parts   = [ ["Base.STL", [90,0,0],[-60.62,60.62,0],[0,0,-56]],            //  no rotation  / 0
            ["Waist.STL",[90,0,0],[-48.5,49.24,0],[4,-14.6585,-40.2765]], // rotate Z + => counterclockwise  // 1
            ["Arm 01.STL",[0,90,0],[0,-25.6757,28.4575],[0,-120,0]],      // rotate X + => up                // 2
            ["Arm 02 v3.STL",[0,-90,0],[6.3782,-24.2497,-19.2875],[13.88-6.3782,-89.75,5.3]], // rotate X + => up      // 3
            ["Arm 03.STL",[0,-90,0],[14,0,-11.5],[-14,-28,-5]],      // rotate Y + => counterclockwise      // 4
            ["Gripper base.STL",[-90,0,0],[-6,-14,14],[-17,-58,14]], // rotate X + => up      // 5
            ["Gripper 1.STL",[0,90,-90],[-5,38.3138,0],[10,0,0]],    // rotate Z + => close   // 6 right
            ["Gripper 1.STL",[0,-90,90],[5,38.3138,-8.5]],           // rotate Z + => open    // 7 left
            //["gear1.STL",[0,0,0],[0,0,0]],   	         // 9
            //["gear2.STL",[0,0,0],[0,0,0]],   	         // 6
            //["grip link 1.STL",[0,0,0],[0,0,0]],        // 7
            ]; 


module importer(i,nextIt=false){ 
  if (nextIt){
    translate(parts[i][3])
      translate(parts[i][2])
        rotate(parts[i][1])
          import(str(prefix,parts[i][0]));
  }
  else{
    translate(parts[i][2])
      rotate(parts[i][1])
        import(str(prefix,parts[i][0]));
  }
 }

module fullArm(){
  translate(parts[6][3]){
    translate(parts[5][3]){
      translate(parts[4][3]){
        translate(parts[3][3]){
          translate(parts[2][3]){
            translate(parts[1][3])
              importer(0,true);
            importer(1,true);
          }
          importer(2,true);
        }
        importer(3,true);
      }
      importer(4,true);
    }
    importer(5,true);
  }  
  importer(6,true);
  importer(7);
}