/*
parts   = [ ["Base.STL", [90,0,0],[-60.62,60.62,0],[0,0,-56]],            //  no rotation  / 0
            ["Waist.STL",[90,0,0],[-48.5,49.24,0],[4,-14.6585,-40.2765]], // rotate Z + => counterclockwise  // 1
            ["Arm 01.STL",[0,90,0],[0,-25.6757,28.4575],[0,-120,0]],      // rotate X + => up                // 2
            ["Arm 02 v3.STL",[0,-90,0],[6.3782,-24.2497,-19.2875],[13.88-6.3782,-89.75,5.3]], // rotate X + => up      // 3
            ["Arm 03.STL",[0,-90,0],[14,0,-11.5],[-14,-28,-5]],      // rotate Y + => counterclockwise      // 4
            ["Gripper base.STL",[-90,0,0],[-6,-14,14],[-17,-58,14]], // rotate X + => up      // 5
            ["Gripper 1.STL",[0,90,-90],[-5,38.3138,0],[10,0,0]],    // rotate Z + => close   // 6 right
            ["Gripper 1.STL",[0,-90,90],[5,38.3138,-8.5]],           // rotate Z + => open    // 7 left
            ]; 
 module importer(i,nextIt=false){ 
  if (nextIt){
    translate(parts[i][3])
      translate(parts[i][2])
        rotate(parts[i][1])
          import(str(prefix,parts[i][0]));
  }
  else{
    translate(parts[i][2])
      rotate(parts[i][1])
        import(str(prefix,parts[i][0]));
  }
 }
 module fullArm(){
  translate(parts[6][3]){
    translate(parts[5][3]){
      translate(parts[4][3]){
        translate(parts[3][3]){
          translate(parts[2][3]){
            translate(parts[1][3])
              importer(0,true);
            importer(1,true);
          }
          importer(2,true);
        }
        importer(3,true);
      }
      importer(4,true);
    }
    importer(5,true);
  }  
  importer(6,true);
  importer(7);
}

*/


String prefix = "../../STL_Arm_parts/OBJ/";

String parts[] = {"Base.obj", 
                  "Waist.obj",
                  "Arm 01.obj",
                  "Arm 02 v3.obj",
                  "Arm 03.obj",
                  "Gripper base.obj",
                  "Gripper 1.obj",
                  "Gripper 1.obj"
};

float positionVec[][][] = {{{PI/2,0,0},{-60.62,60.62,0},{0,0,-56}},            //  no rotation  / 0
                          {{PI/2,0,0},{-48.5,49.24,0},{4,-14.6585,-40.2765}}, // rotate Z + => counterclockwise  // 1
                          {{0,PI/2,0},{0,-25.6757,28.4575},{0,-120,0}},      // rotate X + => up                // 2
                          {{0,-PI/2,0},{6.3782,-24.2497,-19.2875},{13.88-6.3782,-89.75,5.3}}, // rotate X + => up      // 3
                          {{0,-PI/2,0},{14,0,-11.5},{-14,-28,-5}},      // rotate Y + => counterclockwise      // 4
                          {{-PI/2,0,0},{-6,-14,14},{-17,-58,14}},       // rotate X + => up      // 5
                          {{0,PI/2,-PI/2},{-5,38.3138,0},{10,0,0}},     // rotate Z + => close   // 6 right
                          {{0,-PI/2,PI/2},{5,38.3138,-8.5},{0,0,0}}};   // rotate Z + => open    // 7 left

color yellow    = color(255,255,0),
      yellow1   = color(255,255,200),
      red       = color(255,0,0),
      green     = color(0,255,0),
      green1    = color(204, 255, 51),
      blue      = color(0,0,255),
      turquoise = color(0,255,255),
      purple    = color(255,0,255),
      orange    = color(255,165,0);
      

color colorVec[] = {yellow,   
                    //yellow1,  
                    red,      
                    green,    
                    green1,   
                    blue,     
                    turquoise,
                    purple,   
                    orange};   

PShape armI(int i){
  PShape res = loadShape(prefix+parts[i]);
  res.setFill(colorVec[i]);
  return res;
}


float x0,y0;
PShape sVec[];

void setup(){
  size(1200,800,P3D);
  x0=width/2.;
  y0=height/2.;
  lights();
  sVec = new PShape[8];
  for (int i=0;i<parts.length;i++){
    sVec[i] = armI(i);
  }
}

void allEm(){
  for (int i=0;i<parts.length;i++){
    translate(20*i,20*i);
    shape(sVec[i]);
  }
}

void lineIt(){
  pushMatrix();
  pushStyle();
  translate(x0,y0);
  stroke(red);   // X == RED
  line(0,0,0,
       width/2,0,0);
  stroke(green);  // Y == GREEN
  line(0,0,0,
       0,height/2,0);
  stroke(blue);   // Z ++ BLUE
  line(0,0,0,
       0,0,height/2);
  popStyle();
  popMatrix();
}
  

void place(int n){
  pushMatrix();
  translateTriplet(positionVec[n][1]);
  rotateTriplet(positionVec[n][0]);
  shape(sVec[n]);                             // [0][0]
  popMatrix();
}

void translateTriplet(float v[]){
  translate(v[0],v[1],v[2]);
}
void rotateTriplet(float v[]){
  //rotateX(v[0]);              // [0][1]
 // rotateY(v[1]);              // [0][1]
  rotateZ(v[2]);              // [0][1]
  rotateY(v[1]);              // [0][1]
  rotateX(v[0]);              // [0][1]
  
}  

void draw(){
  //  camera(mouseX*2, mouseY*2, (height/2) / tan(PI/6), width/2, height/2, 0, 0, 1, 0);
  camera(0, height/2, (height/2) / tan(PI/6), 
         width/2, height/2, 0, 
         -1, 0, 0);
  background(0);
  lineIt();
  translate(x0,y0);
  /////
  pushMatrix();  // push 0
    translateTriplet(positionVec[7][2]);
    pushMatrix();  // push 1
      translateTriplet(positionVec[6][2]);
      pushMatrix();  // push 2
        translateTriplet(positionVec[5][2]);
        pushMatrix();  // push 3
          translateTriplet(positionVec[4][2]);
          pushMatrix();  // push 4
            translateTriplet(positionVec[3][2]);
            pushMatrix();  // push 5
              translateTriplet(positionVec[2][2]);
              pushMatrix();  // push 6
                translateTriplet(positionVec[1][2]);
                pushMatrix();  // push 7
                  translateTriplet(positionVec[0][2]);
                  place(0);
                popMatrix();  // pop 7
                place(1);
              popMatrix();  // pop 6  
              place(2);
            popMatrix();  // pop 5
            place(3);
          popMatrix();  // pop 4
          place(4);
        popMatrix();  // pop 3
        place(5);
      popMatrix();  // pop 2
      rotateZ(radians(15));
      place(6);
    popMatrix();  // pop 1
    rotateZ(radians(-15));
    place(7);
  popMatrix();  // pop 0
}
