import processing.serial.*;
import java.io.*;

Serial port; 

final int baud      = 115200,
          nbTickers = 7,
          firstTicker = 1;
final String pName = "/home/bob/ttyXYZ",  // we read here!
             cName = "s",                 // "$HOME/ttyABC";  // we write here!
             speedName = "s";
Ticker tickerVec[];
int speedDelay = 50;

void setSpeed(float newfSpeed0to100){
  speedDelay = round(map(newfSpeed0to100,0,180,1000,1));
  println("Speed Delay: ", speedDelay);
}

void setTarget(int i,float val){
  tickerVec[i].setTarget(val);
  println("Set target: ",i, val);
}

void doBuff(String s){  // s of for Cn+nnn or Cn-nnn
  if (s.length() < 6){
    return;
  }
  String commandS = s.substring(0,1),
         idS      = s.substring(1,2),
         valS     = s.substring(2,6);
  println(commandS,idS, valS); 
  if (idS.equals(speedName)){
    setSpeed(Integer.valueOf(valS));
  }
  else if (commandS.equals(cName)){
    setTarget(Integer.valueOf(idS),Integer.valueOf(valS));
  }
  doBuff(s.substring(6,s.length()));
}

void initTickers(){
  for (int i =firstTicker; i< tickerVec.length;i++){
    tickerVec[i] = new Ticker(90);
  }
}

 void setup() {
  tickerVec = new Ticker[8];
  initTickers();
  exec("/home/bob/Desktop/temp/DigiTarn/bras-robotique/Processing/doSocat");
  delay(500);
  port = new Serial(this, pName,baud);
  println("Starting up...");
}  
void draw() {
  while (port.available() > 0) {
    String inBuffer = port.readString();   
    if (inBuffer != null) {
      try{
        doBuff(inBuffer);
      }
      catch(Exception e){
        println("failed",e);
      }
    }
  }
  boolean worksDone=true;
  for (int i =firstTicker; i< tickerVec.length;i++){
    worksDone =  tickerVec[i].update() && worksDone;   
  }
  if(!worksDone){
     for (int i =firstTicker; i< tickerVec.length;i++){
      print(i, 
            str(tickerVec[i].currentVal)  + "/" + str(tickerVec[i].targetVal) ,
            (i<7 ? " : " : "\n"));
    }
  }
  delay(speedDelay);
}
