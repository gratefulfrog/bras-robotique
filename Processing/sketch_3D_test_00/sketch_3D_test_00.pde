
void setup(){
  size(1200,800,P3D);
  noFill();
  camera(width/2,2*height,(height/2.0) / tan(PI*30.0 / 180.0),
         width/2, height/2,  0.0, 
         0, 1, 0);
  frameRate(25);
}

color yellow    = color(255,255,0),
      red       = color(255,0,0),
      green     = color(0,255,0),
      blue      = color(0,0,255),
      turquoise = color(0,255,255),
      purple    = color(255,0,255);

float x = 400,
      y = 250,
      alphaInc = PI/180,
      alpha=0;

void blueYellow(){
  pushMatrix();
  pushStyle();
  fill(blue);
  rect(0,0,x,y/2.);
  translate(0,y/2.0);
  fill(yellow);
  rect(0,0,x,y/2.);
  popStyle();
  popMatrix();
}

void left(){
  pushMatrix();
  rotateY(-PI/2);
  blueYellow();
  popMatrix();
}
void down(){
  blueYellow();
}

void right(){
  pushMatrix();
  translate(x,0);
  left();
  popMatrix();
}
void up(){
  pushMatrix();
  translate(0,0,x);
  down();
  popMatrix();
}
void back(color c){
  pushMatrix();
  pushStyle();
  fill(c,150);
  rotateX(PI/2);
  rect(0,0,x,x);
  popStyle();
  popMatrix();
}
void front(color c){
  pushMatrix();
  pushStyle();
  fill(c,150);
  translate(0,y);
  back(c);
  popStyle();
  popMatrix();
}
      
void draw(){
  pushMatrix();
  background(255);
  translate(width/2,height/2,0);
  rotateZ(alpha);
  left();
  down();
  right();
  up();
  back(blue);
  front(yellow);
  alpha+=alphaInc;
  popMatrix();
}
