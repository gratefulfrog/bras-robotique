
float   x0,y0;
PShape  sVec[];
ArmPart partVec[];
float maxAngleStep = 1;
float incVec[] = {radians(0),  // for test animation, random values
                  radians(random(1,maxAngleStep)),
                  radians(random(1,maxAngleStep)),
                  radians(random(1,maxAngleStep)),
                  radians(random(1,maxAngleStep)),
                  radians(random(1,maxAngleStep)),
                  radians(1),
                 -radians(1)};

void setup(){
  fullScreen(P3D);
  //size(1900,1080,P3D);
  x0=width/2.;
  y0=height/2.;
  partVec =  new ArmPart[8];
  for (int i=0;i<partNameVec.length;i++){
    partVec[i] = new ArmPart(partNameVec[i]+suffix,
                             colorVec[i],
                             defaultAngleVec[i],
                             minMaxAngleVec[i][0],
                             minMaxAngleVec[i][1],
                             positionVec[i],
                             doPushMatrixVec[i]); 
  }
  frameRate(25);
}

void doInc(int i){
  if (!partVec[i].updateRotationAngle(incVec[i])){
    incVec[i] = -incVec[i];
    partVec[i].updateRotationAngle(incVec[i]);
  }
}

void draw(){
  translate(x0,y0,0);
  boolean isOk = true;
  /*
  pushMatrix();
  for (int i=0;i<partVec.length;i++){
    partVec[i].test();
    if (i==5){
      isOk = modelZ(0,38,0) > 10;
    }
  }
  popMatrix();
  */
  if (isOk){
    camera(width/5., height/5., 0.3*(height/2.) / tan(PI/6), 
           width/2., height/2., height/4., 
           0, 0,-1); //1,0);
    showFrame();
    translate(x0,y0,0);
  }
  for (int i=0;i<partVec.length;i++){
    if (isOk){
      partVec[i].display();
    }
    doInc(i);
  }
}
