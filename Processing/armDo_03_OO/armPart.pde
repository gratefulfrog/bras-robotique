class ArmPart{ 
  float initialRotation[],
        initialPosition[],
        nextPostion[],
        rotationAxis[],
        currentRotation,
        defaultRotation,
        minRotation,
        maxRotation;
        
  boolean doPushMatrix;
  
  PShape theShape;
  
  void createShape(String fileName, color c){  
    theShape = loadShape(fileName);
    theShape.setFill(c);
    theShape.rotateX(initialRotation[0]);
    theShape.rotateY(initialRotation[1]);
    zRotate(theShape,initialRotation[2]);
    translateTriplet(theShape,initialPosition);
  }
  
  boolean updateRotationAngle(float deltaAngle){
    float angle = currentRotation + deltaAngle;
    boolean isOk = ((angle >= minRotation) && (angle <= maxRotation));
    if (isOk){
      currentRotation = angle;
    }
    return isOk;
  }
  void test(){
    _display(false);
  }
  void display(){
    _display(true);
  }
  
  void _display(boolean show){
    if (doPushMatrix){
      pushMatrix();
      rotateTriplet(currentRotation,rotationAxis);
      if (show){
        shape(theShape);
      }
      popMatrix();
    }
    else{
      rotateTriplet(currentRotation,rotationAxis);
      if (show){
        shape(theShape);
      }
    }
    translateTriplet(negate(nextPostion));
  }

  ArmPart(String fileName, color c, 
          float dr, float miR, float maR, 
          float ip[], float ir[], float np[], float ra[], boolean dpm){
    initialRotation = ir;
    initialPosition = ip;
    nextPostion     = np;
    rotationAxis    = ra;
    doPushMatrix    = dpm;
    currentRotation = dr;
    defaultRotation = dr;
    minRotation     = dr+miR;
    maxRotation     = dr+maR;
    //println("min: ", degrees(minRotation), " max: ", degrees(maxRotation));
    createShape(fileName, c);
  }
  
  ArmPart(String fileName, color c, 
          float dr, float miR, float maR,
          float fourTuple[][],boolean dpm){
    initialRotation = fourTuple[0];
    initialPosition = fourTuple[1];
    nextPostion     = fourTuple[2];
    rotationAxis    = fourTuple[3];
    doPushMatrix    = dpm;
    currentRotation = dr;
    defaultRotation = dr;
    minRotation     = dr+miR;
    maxRotation     = dr+maR;
    //println("min: ", degrees(minRotation), " max: ", degrees(maxRotation));
    createShape(fileName, c);
  }
}
