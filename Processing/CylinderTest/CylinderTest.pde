color yellow    = color(255,255,0),
      red       = color(255,0,0),
      green     = color(0,255,0),
      blue      = color(0,0,255),
      turquoise = color(0,255,255),
      purple    = color(255,0,255);
      
float R = 200,
      H = 200;
     
void rectIt(int n, float r, float h){
  float x = width/2.,
        y = height/2.,
        alpha = 2*PI/n,
        d = r*sin(alpha/2.);
  translate(x,y);
  pushMatrix();
  circle(0,0,r);
  
  translate(-d/2.,-d/2.);
  for (int i = 0;i<n;i++){
    pushMatrix();
    rotateX(-PI/2.);
    rect(0,0,d,-h); //line(0,0,d,0);
    popMatrix();
    translate(d,0);
    rotate(alpha);
  }
  
  popMatrix();
  translate(0,0,h);
  fill(yellow);
  circle(0,0,r);
}

PShape s;  // The PShape object

void mBox(float x, float y, float z) {
  // Creating a custom PShape as a square, by
  // specifying a series of vertices.
  s = createShape();
  s.beginShape(QUADS);
  s.stroke(blue);
  
  s.fill(yellow);
  s.vertex(0, 0, 0);
  s.vertex(x, 0, 0);
  s.vertex(x, y, 0);
  s.vertex(0, y, 0);
  
  s.fill(green);
  s.vertex(0, 0, 0);
  s.vertex(x, 0, 0);
  s.vertex(x, 0, z);
  s.vertex(0, 0, z);
  
  s.fill(blue);
  s.vertex(0, 0, 0);
  s.vertex(0, y, 0);
  s.vertex(0, y, z);
  s.vertex(0, 0, z);
  
  s.fill(red);
  s.vertex(0, y, 0);
  s.vertex(x, y, 0);
  s.vertex(x, y, z);
  s.vertex(0, y, z);
  
  s.fill(purple);
  s.vertex(x, 0, 0);
  s.vertex(x, y, 0);
  s.vertex(x, y, z);
  s.vertex(x, 0, z);
  
  s.fill(turquoise);
  s.vertex(0, 0, z);
  s.vertex(x, 0, z);
  s.vertex(x, y, z);
  s.vertex(0, y, z);
  
  s.endShape(CLOSE);
}

float X = 200,
      Y = 300,
      Z=  400;

void setup(){
  size(1200,800,P3D);
  background(0);;
  shapeMode(CENTER);
  camera(0,2*height,(height/2.0) / tan(PI*30.0 / 180.0),
         0, height/2,  0.0, 
         0, 1, 0);
  //frameRate(25);
  pushMatrix();
  translate(width/2.,height/2.);
  mBox(X,Y,Z);
  popMatrix();
  //rectIt(12,R,H);
}

int x = 0;
void draw(){
  background(0);
  stroke(255);
  stroke(blue);
  line(width/2.,height/2.,width,height/2.);
  stroke(green);
  line(width/2.,height/2.,width/2.,height);
  stroke(red);
  line(width/2.,height/2.,0,width/2.,height/2.,800);
  pushMatrix();
  translate(width/2,height/2,-Z/2.);
  rotateX(radians(x));
  rotateY(radians(x));
  rotateZ(radians(x++));
  shape(s,0,0);
  popMatrix();
  
};
