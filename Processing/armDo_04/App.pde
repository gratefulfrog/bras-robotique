class App{
  Comms comms;
  ArmPart partVec[];
  int speedDelay = 0,
      runIndex   = 0;
  ArrayList<float[]> positionList;
  boolean worksDone = false,
          running   = false;
  
  App(PApplet parent){
    comms = new Comms(parent);
    partVec =  new ArmPart[8];
    for (int i=0;i<partNameVec.length;i++){
      partVec[i] = new ArmPart(partNameVec[i]+suffix,
                               colorVec[i],
                               defaultAngleVec[i],
                               minMaxAngleVec[i][0],
                               minMaxAngleVec[i][1],
                               positionVec[i],
                               doPushMatrixVec[i],
                               signAngleCorrectionVec[i],
                               i==2); 
    }
   doReset(); 
  }
  void doReset(){
    running = false;
    positionList = new ArrayList<float[]>() ;
  }
  void doSave(){
    float posVec[] = new float[partVec.length]; 
    for (int i=0;i<partVec.length;i++){
      posVec[i] = partVec[i].getCurrentRotationAngle();
    }
    positionList.add(posVec);
    print("added: ");
    printAngleVec(posVec);
  }
  void doRun(){
    runIndex = 0;
    if(0==positionList.size()){
      return;  // don't set running or do anything if there's nothing to do!
    }
    else if (running){
      running = false;
    }
    else{
      for (int i = 0; i < positionList.size(); i++) {
        float vec[] = positionList.get(i);
        printAngleVec(vec);
      }
      running = true;
    }
  }
   
  void doPause(){
    running = !running;
  }
  
  void stepRunner(){
    if (runIndex == positionList.size()){
      println("cycle completed!"); //running = false;
      runIndex = 0;
    }
    else{
      float vec[] = positionList.get(runIndex++);
        printAngleVec(vec);
        for (int i=0;i<vec.length;i++){
          float a = signAngleCorrectionVec[i]*vec[i];
          partVec[i].setTargetRotation((partVec[i].isShoulder ? radians(90) + a : a));
        }
    }
  }
   
  void display(){
    comms.update();
    boolean worksDone = true;
    for (int i=0;i<partVec.length;i++){
      partVec[i].display();
      worksDone = partVec[i].update() && worksDone;
      //doInc(i);
    }
    if (worksDone && running){
      println("stepping array");
      stepRunner();
    }
    delay(speedDelay);
  }
  void doRead(){
    dataFileName="";
    getDataFile();
    while(dataFileName.equals("")){delay(100);}
    if (dataFileName.equals("##~~")){
      return;
    }
    println("reading from disk");
    String[] lines = loadStrings(dataFileName);
    if (lines == null){
      return;
    }
    doReset();
    for (int i=0;i<lines.length;i++){
      String[] posSVec = split(lines[i], ',');
      float posFVec[] = new float[posSVec.length];      
      for(int j =0;j<posFVec.length;j++){
        posFVec[j] = radians(Float.valueOf(posSVec[j]));
        }
      positionList.add(posFVec);
      print("read: ");
      printAngleVec(posFVec);
    }
  }
  
  void doWrite(){
    dataFileName = "";
    getDataWriteName();
    while(dataFileName.equals("")){delay(100);}
    if (dataFileName.equals("##~~")){
      return;
    }
    println("writing to disk");
    String[] lines = new String[positionList.size()];
    for (int i = 0; i < lines.length; i++) {
      lines[i] = new String("");
      for (int j=0;j<positionList.get(i).length;j++){
        lines[i] += str(roundP(degrees(positionList.get(i)[j]),1)) + (j==positionList.get(i).length-1 ? "" : ",");
      }
      println("wrote: ", lines[i].replace(',',' '));
    }
    saveStrings(dataFileName, lines);
  }
}
