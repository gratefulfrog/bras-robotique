final String suffix = ".obj",
             partNameVec[] = {"Base", 
                              "Waist",
                              "Arm 01",
                              "Arm 02 v3",
                              "Arm 03",
                              "Gripper base",
                              "Gripper 1",
                              "Gripper 1"};

final float nullRot[] = {0,0,0},
            xRot[]    = {1,0,0},
            yRot[]    = {0,1,0},
            zRot[]    = {0,0,1},
            //positionVec[8][4][3] // inital rotation vector, initial position vector, translation for next pivot, active rotatation vector
            positionVec[][][] = {{{PI/2.,0,0},{-60.62,60.62,0},{0,0,-56},nullRot,},         //  no rotation                     // 0
                                 {{PI/2.,0,0},{-48.5,49.24,0},{4,-14.6585,-40.2765},zRot}, // rotate Z + => counterclockwise   // 1
                                 {{0,PI/2.,0},{0,-25.6757,28.4575},{0,-120,0},xRot},       // rotate X + => up                 // 2
                                 {{0,-PI/2.,0},{6.3782,-24.2497,-19.2875},{13.88-6.3782,-89.75,5.3},xRot}, // rotate X + => up // 3
                                 {{0,-PI/2.,0},{14,0,-11.5},{-14,-28,-5},yRot},            // rotate Y + => counterclockwise   // 4
                                 {{-PI/2.,0,0},{-6,-14,14},{-17,-58,14},xRot},             // rotate X + => up                 // 5
                                 {{0,PI/2.,-PI/2.},{-5,38.3138,0},{10,0,0},zRot},          // rotate Z + => close              // 6 right grip
                                 {{0,-PI/2.,PI/2.},{5,38.3138,-8.5},{0,0,0},zRot}};        // rotate Z + => open               // 7 left  grip

final float signAngleCorrectionVec[] = {0,   // 0 base no rotion,no coord rotation
                                        1,   // 1 waist no correction
                                       -1,   // 2 shoulder invert
                                       -1,   // 3 elbow invert
                                        1,   // 4 wrist roll no correcton
                                       -1,   // 5 wrist elevation invert
                                        1,   // 6 right grip no correction
                                        1};  // 7 left grip no correction

final boolean doPushMatrixVec[] = {false,
                                   false,
                                   false,
                                   false,
                                   false,
                                   false,
                                   true,
                                   false};
                                   
final color yellow    = color(255, 255,   0),
            yellow1   = color(255, 255, 200),
            red       = color(255,   0,   0),
            green     = color(  0, 255,   0),
            green1    = color(204, 255,  51),
            blue      = color(  0,   0, 255),
            turquoise = color(  0, 255, 255),
            purple    = color(255,   0, 255),
            orange    = color(255, 165,   0),
            colorVec[] = {yellow,   
                          red,      
                          green,    
                          green1,   
                          blue,     
                          turquoise,
                          purple,   
                          orange};   

final float defaultAngleVec[] = {radians(0),      // 0 base no rotation
                                 radians(0),      // 1 waist
                                 radians(90),     // 2 shoulder
                                 radians(0),      // 3 elbow
                                 radians(0),      // 4 wrist roll
                                 radians(0),      // 5 wirst elevation
                                 radians(0),      // 6 left grip
                                 radians(-0)},    // 7 right grip
            gripperMaxAngle    = radians(21),
            gripperMinAngle   =  radians(0),
            epsilonAngle       = radians(20),
            minMaxAngleVec[][] = {{0,0},                                // 0 base no rotation
                                  {-PI,PI},                             // 1 waist full rotation
                                  {-PI/2.,PI/2.},                       // 2 shoulder stop at horizontal rotation
                                  {-PI+epsilonAngle,PI-epsilonAngle},   // 3 elbow  nearly full rotation
                                  {-PI,PI},                             // 4 wrist roll full rotation
                                  {-PI+epsilonAngle,PI-epsilonAngle},   // wirst elevation  nearly full rotation
                                  {-gripperMinAngle,gripperMaxAngle},   // 6 left grip as set
                                  {-gripperMaxAngle,gripperMinAngle}},  // 7 right grip as set
            lenArm1 = 120,
            lenArm2 = 90;

char commandCharVec[] = { 's',  // 0 Speed
                          'v',  // 1 saVe
                          'r',  // 2 Reset
                          'p',  // 3 Pause
                          'n'}; // 4 ruN

/*  values from STL files analyzed in openscad and librecad:
parts   = [ ["Base.STL", [90,0,0],[-60.62,60.62,0],[0,0,-56]],            //  no rotation  / 0
            ["Waist.STL",[90,0,0],[-48.5,49.24,0],[4,-14.6585,-40.2765]], // rotate Z + => counterclockwise  // 1
            ["Arm 01.STL",[0,90,0],[0,-25.6757,28.4575],[0,-120,0]],      // rotate X + => up                // 2
            ["Arm 02 v3.STL",[0,-90,0],[6.3782,-24.2497,-19.2875],[13.88-6.3782,-89.75,5.3]], // rotate X + => up      // 3
            ["Arm 03.STL",[0,-90,0],[14,0,-11.5],[-14,-28,-5]],      // rotate Y + => counterclockwise      // 4
            ["Gripper base.STL",[-90,0,0],[-6,-14,14],[-17,-58,14]], // rotate X + => up      // 5
            ["Gripper 1.STL",[0,90,-90],[-5,38.3138,0],[10,0,0]],    // rotate Z + => close   // 6 right
            ["Gripper 1.STL",[0,-90,90],[5,38.3138,-8.5]],           // rotate Z + => open    // 7 left
            ];  
*/
