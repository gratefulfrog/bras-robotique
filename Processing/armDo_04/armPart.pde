class ArmPart{ 
  float initialRotation[],
        initialPosition[],
        nextPostion[],
        rotationAxis[],
        defaultRotation,
        rotationSign,
        minRotation,
        maxRotation;

  Ticker  currentRotation;
  boolean doPushMatrix;
  boolean isShoulder;
  
  PShape theShape;
  
  void createShape(String fileName, color c){  
    theShape = loadShape(fileName);
    theShape.setFill(c);
    theShape.rotateX(initialRotation[0]);
    theShape.rotateY(initialRotation[1]);
    zRotate(theShape,initialRotation[2]);
    translateTriplet(theShape,initialPosition);
  }
  
  boolean updateRotationAngle(float deltaAngle){
    float angle = currentRotation.get() + deltaAngle;
    boolean isOk = ((angle >= minRotation) && (angle <= maxRotation));
    if (isOk){
	    currentRotation.setTarget(angle);
    }
    return isOk;
  }
  void test(){
    _display(false);
  }
  void display(){
    _display(true);
  }
  
  void _display(boolean show){
    if (doPushMatrix){
      pushMatrix();
      rotateTriplet(currentRotation.get(),rotationAxis);
      if (show){
        shape(theShape);
      }
      popMatrix();
    }
    else{
      
	    rotateTriplet(currentRotation.get(),rotationAxis);
      if (show){
        shape(theShape);
      }
    }
    translateTriplet(negate(nextPostion));
  }

  ArmPart(String fileName, color c, 
          float dr, float miR, float maR, 
          float ip[], float ir[], float np[], float ra[], boolean dpm,
          float cs, boolean is){
    initialRotation = ir;
    initialPosition = ip;
    nextPostion     = np;
    rotationAxis    = ra;
    doPushMatrix    = dpm;
    currentRotation = new Ticker(dr);
    defaultRotation = dr;
    minRotation     = dr+miR;
    maxRotation     = dr+maR;
    rotationSign    = cs;
    isShoulder      = is;
    //println("min: ", degrees(minRotation), " max: ", degrees(maxRotation));
    createShape(fileName, c);
  }
  
  ArmPart(String fileName, color c, 
          float dr, float miR, float maR,
          float fourTuple[][],boolean dpm,
          float cs,boolean is){
    initialRotation = fourTuple[0];
    initialPosition = fourTuple[1];
    nextPostion     = fourTuple[2];
    rotationAxis    = fourTuple[3];
    doPushMatrix    = dpm;
    currentRotation = new Ticker(dr);
    defaultRotation = dr;
    minRotation     = dr+miR;
    maxRotation     = dr+maR;
    rotationSign    = cs;
    isShoulder      = is;
    //println("min: ", degrees(minRotation), " max: ", degrees(maxRotation));
    createShape(fileName, c);
  }
  void setTargetRotation(float targetAngle){
    if (isShoulder){
      targetAngle -=radians(90);
    }
    currentRotation.setTarget(targetAngle*rotationSign); //+defaultRotation);
  }
  boolean update(){
    // return true if worksdone!
    return currentRotation.update();
  }
  float getCurrentRotationAngle(){
    return currentRotation.get();
  }
}
