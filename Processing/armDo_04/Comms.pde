import processing.serial.*;
import java.io.*;

class Comms{
  Process guiProcess;
  Serial port; 
  final int baud      = 115200;
  final String pName = "/home/bob/ttyXYZ",  // we read here!
               cName = "s",                 // "$HOME/ttyABC";  // we write here!
               speedName = "s";
  int speedDelay = 50;
  int counter =0;
  
  void setSpeed(float newfSpeed0to100){
    app.speedDelay = round(map(newfSpeed0to100,0,180,20,0));
    println("Speed Delay: ", app.speedDelay);
  }
  
  void setTarget(int i,float val){
    app.partVec[i].setTargetRotation(radians(val));
    //app.partVec[i].updateRotationAngle(radians(val));
    if (i==6){
      app.partVec[7].setTargetRotation(radians(-val));
    }
    println("Set target: ",i, val);
  }
  
  void doBuff(String s){  // s of for Cn+nnn or Cn-nnn
    if (s.length() < 6){
      return;
    }
    String commandS = s.substring(0,1),
           idS      = s.substring(1,2),
           valS     = s.substring(2,6);
    println(counter++, " : ", commandS,idS, valS); 
    execCommandChar(idS.charAt(0),Integer.valueOf(valS));
    /*
    if (idS.equals(speedName)){
      setSpeed(Integer.valueOf(valS));
    }
    else if (commandS.equals(cName)){
      setTarget(Integer.valueOf(idS),Integer.valueOf(valS));
    }
    */
    doBuff(s.substring(6,s.length()));
  }
  
  void execCommandChar(char c, int arg){
    switch (c){
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
        setTarget(Integer.valueOf(str(c)),arg);
        break;
      case 's':
        setSpeed(arg);
        break;
      case 'v': 
        app.doSave();
        break;
      case 'r':
        app.doReset();
        break;
      case 'p':
        app.doPause();
        break;
      case 'n':
        app.doRun();
        break;
      case 'd': // read from disk
        app.doRead();
        break;
      case 'w': // write to disk
        app.doWrite();
        break;
    }
  }  
  
  Comms(PApplet parent) {
    exec("/home/bob/Desktop/temp/DigiTarn/bras-robotique/Processing/doSocat");
    delay(500);
    port = new Serial(parent, pName,baud);
    //guiProcess = exec("/home/bob/Desktop/temp/DigiTarn/bras-robotique/Python/simGui.py");
    guiProcess = exec("/home/bob/Desktop/temp/DigiTarn/bras-robotique/Python/simGui_Nunchuck_BrasRunner_00.py");
    println("Starting up...");
  }  
  void onExit(){
    guiProcess.destroy();
  }
  
  void update() {
    while (port.available() > 0) {
      String inBuffer = port.readString();   
      if (inBuffer != null) {
        try{
          doBuff(inBuffer);
        }
        catch(Exception e){
          println("failed",e);
        }
      }
    }
  }
}
