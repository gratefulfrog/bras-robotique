color yellow    = color(255,255,0),
      red       = color(255,0,0),
      green     = color(0,255,0),
      blue      = color(0,0,255),
      turquoise = color(0,255,255),
      purple    = color(255,0,255);

PShape sN,sN1,sN2;

PShape arm_03(){
  //PShape res = loadShape("Arm 03 repositioned_base_INPUT_HOLE.obj");
  res.setFill(yellow);
  res.rotateY(PI/2.);
  res.rotateX(PI/2.);
  return res;
}



PShape arm_02(){
  PShape res = loadShape("Arm 02 repositioned_base.obj");
  res.setFill(turquoise);
  res.rotateY(PI/2.);
  res.rotateX(PI/2.);
  return res;
}


PShape arm_01(){
  PShape res = loadShape("Arm 01 repositioned_base.obj");
  res.setFill(purple);
  res.rotateY(PI/2.);
  res.rotateX(PI/2.);
  return res;
}

PShape bx(float x,float y, float z, float alpha, color c){
  PShape res = createShape(BOX,x,y,z);
  res.setFill(c);
  //res.rotateX(alpha);
  return res;
}


PShape cylIt(float r, float h, int n, color c){
  PShape res  = createShape(GROUP);
  float trackW = -r/5.,
        trackH = 1.95*r;
  PShape topTrack =  triEqui(trackH,trackW, color(0,0,0));
  topTrack.translate(0,0,h+1);
  topTrack.setFill(color(0,0,0));
  PShape base = triIt(r,n,color(0,255,0),0);
  PShape top  = triIt(r,n,color(0,0,255),h);
  PShape side = quadIt(r,n,color(255,0,0),h);
  res.addChild(base);
  res.addChild(top);
  res.addChild(side);
  res.addChild(topTrack);
  return res;
}

PShape triEqui(float h,float b, color c){
  PShape s = createShape();
  s.beginShape(TRIANGLES);
  s.noStroke();
  s.fill(c);
  s.vertex(0,-h/2);
  s.vertex(-b/2,h/2);
  s.vertex(b/2,h/2);
  s.endShape();
  return s;
}
  

PShape quadIt(float r, int n, color c, float z){
  PShape s = createShape();
  s.beginShape(QUAD_STRIP);
  s.noStroke();
  s.fill(c);
  float alpha = 2*PI/n;
  for (int i = 0; i<n+1;i++){
    s.vertex(r*cos(alpha*i),r*sin(alpha*i),0);
    s.vertex(r*cos(alpha*i),r*sin(alpha*i),z);
  }
  s.endShape();
  return s;
}

PShape triIt(float r, int n, color c, float z){
  PShape s = createShape();
  s.beginShape(TRIANGLE_FAN);
  s.noStroke();
  s.fill(c);
  s.vertex(0, 0,z);
  float alpha = 2*PI/n;
  for (int i = 0; i<n;i++){
    s.vertex(r*cos(alpha*i),r*sin(alpha*i),z);
    //println(r*cos(alpha*i),r*sin(alpha*i),z);
  }
  s.vertex(r, 0,z); 
  s.endShape();
  return s;
}

float x0,y0;

void setup(){
  size(1200,800,P3D);
  x0=width/2.;
  y0=height/2.;
  lights();
  camera(width/2, height, (height/2) / tan(PI/6), width/2, height/2, 0, 0, 1, 0);
  //sN = cylIt(100,200,24,color(0,255,0));
  //sN1 = bx(100,100,300,radians(45),purple);
  sN = arm_01();
  sN1 = arm_02();
  sN2 = arm_03();
  frameRate(5);
}

int x=0;
void draw(){
  background(0);
  camera(mouseX, mouseY, (height/2) / tan(PI/6), width/2, height/2, 0, 0, 1, 0);
  translate(x0, y0);
  //rotate(radians(x++));
  //rotateX(radians(map(mouseX,0,width,-180,180)));
  shape(sN);
  translate(0,0,145.6760);
  //rotateX(radians(map(mouseY,0,height,0,-180)));
  shape(sN1);
  //translate(11.5//,0,115-25.25);
  translate(0,0,115-25.25);
  //rotateZ(x++);
  shape(sN2);
  //delay(1000);
}

/*

void draw(){
  background(0);
  translate(x0, y0);
  shape(sN1);
}
*/
 
