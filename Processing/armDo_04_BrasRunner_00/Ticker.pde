class Ticker{
  float currentVal,
        targetVal,
        inc = radians(1),
        sign = 1;
  Ticker(float cv){
    currentVal = cv;
    targetVal  = cv;
  }
  void setInc(float inC){
    inc = inC;
  }
  void setTarget(float t){
    targetVal = t;
    if (abs(targetVal-currentVal)<abs(inc)){
      currentVal = targetVal;
      return;
    }
    else{
      sign = abs(currentVal-targetVal)/(currentVal-targetVal);
      inc = -abs(inc)*sign;
    }
  }
  float get(){
    return currentVal;
  }
  boolean update(){
    boolean worksDone = (abs(currentVal-targetVal)< abs(inc)); // true means done
    if (!worksDone){
      currentVal +=inc;
    }
    else{
      currentVal=targetVal;
    }
    return worksDone;
  }
}
