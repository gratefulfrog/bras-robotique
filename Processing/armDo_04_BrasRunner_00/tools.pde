void zRotate(float angle){
  //s.rotateZ(angle);  // this is defective and needs the following workaround!
  //s.rotate(angle, 0, 0, 1);
  rotateTriplet(angle,zRot);
}
void zRotate(PShape s, float angle){
  //s.rotateZ(angle);  // this is defective and needs the following workaround!
  //s.rotate(angle, 0, 0, 1);
  rotateTriplet(s,angle,zRot);
}
void rotateTriplet(float angle, float t[]){
  rotate(angle,t[0],t[1],t[2]);
}
void rotateTriplet(PShape s, float angle, float t[]){
  s.rotate(angle,t[0],t[1],t[2]);
}
void translateTriplet(float t[]){
  translate(t[0],t[1],t[2]);
}
void translateTriplet(PShape s, float t[]){
  s.translate(t[0],t[1],t[2]);
}
float[] negate(float v[]){
  float res[] = new float [v.length];
  for (int i = 0;i< v.length;i++){
    res[i] = -v[i];
  }
  return res;
}

void lineIt(){
  final color cVec[]   = {red,green,blue};  // X == RED; Y == GREEN; Z == BLUE
  final float pVec[][] = {{width/2.,0,0},
                          {0,height/2.,0},
                          {0,0,height/2.}};
  pushMatrix();
  pushStyle();
  translate(x0,y0);
  for (int i = 0; i< pVec.length;i++){
    stroke(cVec[i]);
    line(-pVec[i][0],-pVec[i][1],-pVec[i][2], 
          pVec[i][0], pVec[i][1], pVec[i][2]);
  }
  popStyle();
  popMatrix();
}


void showFrame(){
  background(0);
  lights();
  lineIt();
  pushStyle();
  fill(100);
  rect(0,0,width,height);
  popStyle();
}

void printAngleVec(float vec[]){
  for (int i=0;i<vec.length;i++){
    print(roundP(degrees(vec[i]),1), (i== vec.length-1 ? '\n' : " "));
  }
}
float roundP(float f, int p){
  int m = round(f*pow(10,p));
  return m/pow(10,p);
}

void getDataFile(){
    selectInput("Select a file to process:", "fileSelected");
}
void getDataWriteName(){
  selectOutput("Select a file to write to:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    dataFileName = "##~~";
  } else {
    println("User selected " + selection.getAbsolutePath());
    dataFileName = selection.getAbsolutePath();
  }
}
