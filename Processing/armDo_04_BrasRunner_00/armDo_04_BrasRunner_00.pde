
float   x0,y0;
PShape  sVec[];
ArmPart partVec[];
App app;
String dataFileName = ""; //positions.data";

void setup(){
  //fullScreen(P3D);
  size(1350,1000,P3D);
  x0=width/2.;
  y0=height/2.;
  app = new App(this);
  //frameRate(25);
}

void draw(){
  translate(x0,y0,0);
  /*
  camera(width/5., height/5., 0.3*(height/2.) / tan(PI/6), 
         width/2., height/2., height/4., 
         0, 0,-1); //1,0);
  */
  camera(width/10., height/10., 0.3*(height/2.) / tan(PI/6), 
         width/2., height/2., height/4., 
         0, 0,-1); //1,0);
  showFrame();
  translate(x0,y0,0);
  app.display();
}

void exit() {
  println("exiting");
  app.comms.onExit();
  super.exit();
}
void stop(){
  this.exit();
}
