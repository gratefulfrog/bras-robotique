/*  values from STL files analyzed in openscad and librecad:
parts   = [ ["Base.STL", [90,0,0],[-60.62,60.62,0],[0,0,-56]],            //  no rotation  / 0
            ["Waist.STL",[90,0,0],[-48.5,49.24,0],[4,-14.6585,-40.2765]], // rotate Z + => counterclockwise  // 1
            ["Arm 01.STL",[0,90,0],[0,-25.6757,28.4575],[0,-120,0]],      // rotate X + => up                // 2
            ["Arm 02 v3.STL",[0,-90,0],[6.3782,-24.2497,-19.2875],[13.88-6.3782,-89.75,5.3]], // rotate X + => up      // 3
            ["Arm 03.STL",[0,-90,0],[14,0,-11.5],[-14,-28,-5]],      // rotate Y + => counterclockwise      // 4
            ["Gripper base.STL",[-90,0,0],[-6,-14,14],[-17,-58,14]], // rotate X + => up      // 5
            ["Gripper 1.STL",[0,90,-90],[-5,38.3138,0],[10,0,0]],    // rotate Z + => close   // 6 right
            ["Gripper 1.STL",[0,-90,90],[5,38.3138,-8.5]],           // rotate Z + => open    // 7 left
            ];  
*/

String prefix = "../../STL_Arm_parts/OBJ/",
       suffix = ".obj",
       partVec[] = {"Base", 
                    "Waist",
                    "Arm 01",
                    "Arm 02 v3",
                    "Arm 03",
                    "Gripper base",
                    "Gripper 1",
                    "Gripper 1"};

float nullRot[] = {0,0,0},
      xRot[]    = {1,0,0},
      yRot[]    = {0,1,0},
      zRot[]    = {0,0,1},
      //positionVec[8][4][3] // inital rotation vector, initial position vector, translation for next pivot, active rotatation vector
      positionVec[][][] = {{{PI/2.,0,0},{-60.62,60.62,0},{0,0,-56},nullRot},         //  no rotation                     // 0
                           {{PI/2.,0,0},{-48.5,49.24,0},{4,-14.6585,-40.2765},zRot}, // rotate Z + => counterclockwise   // 1
                           {{0,PI/2.,0},{0,-25.6757,28.4575},{0,-120,0},xRot},       // rotate X + => up                 // 2
                           {{0,-PI/2.,0},{6.3782,-24.2497,-19.2875},{13.88-6.3782,-89.75,5.3},xRot}, // rotate X + => up // 3
                           {{0,-PI/2.,0},{14,0,-11.5},{-14,-28,-5},yRot},            // rotate Y + => counterclockwise   // 4
                           {{-PI/2.,0,0},{-6,-14,14},{-17,-58,14},xRot},             // rotate X + => up                 // 5
                           {{0,PI/2.,-PI/2.},{-5,38.3138,0},{10,0,0},zRot},          // rotate Z + => close              // 6 right grip
                           {{0,-PI/2.,PI/2.},{5,38.3138,-8.5},{0,0,0},zRot}};        // rotate Z + => open               // 7 left  grip

color yellow    = color(255, 255,   0),
      yellow1   = color(255, 255, 200),
      red       = color(255,   0,   0),
      green     = color(  0, 255,   0),
      green1    = color(204, 255,  51),
      blue      = color(  0,   0, 255),
      turquoise = color(  0, 255, 255),
      purple    = color(255,   0, 255),
      orange    = color(255, 165,   0),
      colorVec[] = {yellow,   
                    red,      
                    green,    
                    green1,   
                    blue,     
                    turquoise,
                    purple,   
                    orange};   

float x0,y0,
      rotationVec[],
      defaultAngleVec[] = {radians(0),      // 0 base no rotation
                           radians(0),      // 1 waist
                           radians(90),     // 2 shoulder
                           radians(0),      // 3 elbow
                           radians(0),      // 4 wrist roll
                           radians(0),      // 5 wirst elevation
                           radians(0),      // 6 left grip
                           radians(-0)},    // 7 right grip
      incVec[] = {radians(0),  // for test animation, random values
                  radians(random(1,10)),
                  radians(random(1,10)),
                  radians(random(1,10)),
                  radians(random(1,10)),
                  radians(random(1,10)),
                  radians(1),
                 -radians(1)};
PShape sVec[];

void zRotate(float angle){
  //s.rotateZ(angle);  // this is defective and needs the following workaround!
  //s.rotate(angle, 0, 0, 1);
  rotateTriplet(angle,zRot);
}
void zRotate(PShape s, float angle){
  //s.rotateZ(angle);  // this is defective and needs the following workaround!
  //s.rotate(angle, 0, 0, 1);
  rotateTriplet(s,angle,zRot);
}
void rotateTriplet(float angle, float t[]){
  rotate(angle,t[0],t[1],t[2]);
}
void rotateTriplet(PShape s, float angle, float t[]){
  s.rotate(angle,t[0],t[1],t[2]);
}
void translateTriplet(float t[]){
  translate(t[0],t[1],t[2]);
}
void translateTriplet(PShape s, float t[]){
  s.translate(t[0],t[1],t[2]);
}
float[] negate(float v[]){
  float res[] = new float [v.length];
  for (int i = 0;i< v.length;i++){
    res[i] = -v[i];
  }
  return res;
}

PShape armI(int i){
  PShape res = loadShape(prefix+partVec[i]+suffix);
  res.setFill(colorVec[i]);
  res.rotateX(positionVec[i][0][0]);
  res.rotateY(positionVec[i][0][1]);
  zRotate(res,positionVec[i][0][2]);
  translateTriplet(res,positionVec[i][1]);
  return res;
}

void setup(){
  fullScreen(P3D);
  x0=width/2.;
  y0=height/2.;
  initRotationVec();
  sVec = new PShape[8];
  for (int i=0;i<partVec.length;i++){
    sVec[i] = armI(i);
  }
  frameRate(25);
}

void initRotationVec(){
  rotationVec = new float[defaultAngleVec.length];
  for (int i= 0;i < rotationVec.length;i++){
    rotationVec[i] = defaultAngleVec[i];
  }
}
  
void doInc(){
  for (int i=1; i < incVec.length;i++){
    rotationVec[i]+=incVec[i];
    if (   (rotationVec[i] <  (i > 5 ? radians(-20)  : defaultAngleVec[i]-PI/2.)) 
        || (rotationVec[i] > (i>5 ? radians(20) : defaultAngleVec[i]+PI/2.))){
      incVec[i] = -incVec[i];
    }
  }
}

void rotatePart(int i){
  rotateTriplet(rotationVec[i],positionVec[i][3]);
}
 
void displayPart(int i, boolean pop){
  if (pop){
    pushMatrix();
    rotatePart(i);
    shape(sVec[i]);
    popMatrix();
  }
  else{
    rotatePart(i);
    shape(sVec[i]);
  }
  translateTriplet(negate(positionVec[i][2]));
}

void lineIt(){
  final color cVec[]   = {red,green,blue};  // X == RED; Y == GREEN; Z == BLUE
  final float pVec[][] = {{width/2.,0,0},
                          {0,height/2.,0},
                          {0,0,height/2.}};
  pushMatrix();
  pushStyle();
  translate(x0,y0);
  for (int i = 0; i< pVec.length;i++){
    stroke(cVec[i]);
    line(-pVec[i][0],-pVec[i][1],-pVec[i][2], 
          pVec[i][0], pVec[i][1], pVec[i][2]);
  }
  popStyle();
  popMatrix();
}

void showFrame(){
  background(0);
  lights();
  lineIt();
  pushStyle();
  fill(100);
  rect(0,0,width,height);
  popStyle();
 }
                         
void draw(){
  camera(width/5., height/5., 0.3*(height/2.) / tan(PI/6), 
         width/2., height/2., height/4., 
         0, 0,-1); //1,0);
  showFrame();
  translate(x0,y0,0);
  pushMatrix();
  for (int i=0;i<partVec.length;i++){
    displayPart(i,i==6);
  }
  popMatrix();
  doInc();
}
