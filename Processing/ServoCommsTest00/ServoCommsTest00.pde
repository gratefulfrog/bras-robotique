
final int nbServos = 6;
final String zCommand = "z;";

Comms commsMgr;

float x0,
      lastMouseX;

void setup(){
  size(500,100,P3D);
  x0 = width/2.;
  commsMgr = new Comms(this);
}

void draw(){
  String outgoing = "";
  float        sVal = mouseX;
  if (sVal == lastMouseX){
    return;
  }
  lastMouseX = sVal;
  for (int index = 0; index < nbServos;index++){    
    int ssVal = constrain(round(map(sVal,0,width,0,100)),0,100); //<>//
    outgoing += str(  ssVal) + ",";    
  }
  outgoing +=";";
  println(outgoing);
  commsMgr.sendAndWait(outgoing);
}

void mousePressed(){
  commsMgr.sendAndWait(zCommand);
}
  
