import processing.serial.*;
import java.io.*;

class Comms{
  /*
  final int rangeVec[][] = {{-180,180}, //0  // these are ranges on the simulation
                            {-180,180}, //1
                            {-160,180}, //2
                            {-180,180}, //3
                            {-150,150}, //4
                            {0,21}};    //5
  */
  final int baud      = 115200;
  final String outPortName = "/dev/ttyACM0";
  Serial outPort;
  
  int getTarget(int pct){  // pct is on[0,100]
    //float outVal = map(val,rangeVec[i][0],rangeVec[i][1],0,180);
    //println("Set target: ",i  ,round(outVal));
    return round(pct);
  }
  void sendAndWait(String outgoing){
    outPort.write(outgoing);
    while (outPort.available()<0);
    char reply = outPort.readChar();
    println(reply);
  }
  
  Comms(PApplet parent) {
    outPort = new Serial(parent, outPortName,baud);
    println("Starting up...");
  }  
  void onExit(){
    outPort.stop();
  }
}
