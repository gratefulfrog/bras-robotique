/*
parts   = [ ["Base.STL", [90,0,0],[-60.62,60.62,0],[0,0,-56]],            //  no rotation  / 0
            ["Waist.STL",[90,0,0],[-48.5,49.24,0],[4,-14.6585,-40.2765]], // rotate Z + => counterclockwise  // 1
            ["Arm 01.STL",[0,90,0],[0,-25.6757,28.4575],[0,-120,0]],      // rotate X + => up                // 2
            ["Arm 02 v3.STL",[0,-90,0],[6.3782,-24.2497,-19.2875],[13.88-6.3782,-89.75,5.3]], // rotate X + => up      // 3
            ["Arm 03.STL",[0,-90,0],[14,0,-11.5],[-14,-28,-5]],      // rotate Y + => counterclockwise      // 4
            ["Gripper base.STL",[-90,0,0],[-6,-14,14],[-17,-58,14]], // rotate X + => up      // 5
            ["Gripper 1.STL",[0,90,-90],[-5,38.3138,0],[10,0,0]],    // rotate Z + => close   // 6 right
            ["Gripper 1.STL",[0,-90,90],[5,38.3138,-8.5]],           // rotate Z + => open    // 7 left
            ]; 
 module importer(i,nextIt=false){ 
  if (nextIt){
    translate(parts[i][3])
      translate(parts[i][2])
        rotate(parts[i][1])
          import(str(prefix,parts[i][0]));
  }
  else{
    translate(parts[i][2])
      rotate(parts[i][1])
        import(str(prefix,parts[i][0]));
  }
 }
 module fullArm(){
  translate(parts[6][3]){
    translate(parts[5][3]){
      translate(parts[4][3]){
        translate(parts[3][3]){
          translate(parts[2][3]){
            translate(parts[1][3])
              importer(0,true);
            importer(1,true);
          }
          importer(2,true);
        }
        importer(3,true);
      }
      importer(4,true);
    }
    importer(5,true);
  }  
  importer(6,true);
  importer(7);
}

*/


String prefix = "../../STL_Arm_parts/OBJ/";

String partVec[] = {"Base.obj", 
                    "Waist.obj",
                    "Arm 01.obj",
                    "Arm 02 v3.obj",
                    "Arm 03.obj",
                    "Gripper base.obj",
                    "Gripper 1.obj",
                    "Gripper 1.obj"
};

float positionVec[][][] = {{{PI/2.,0,0},{-60.62,60.62,0},{0,0,-56}},            //  no rotation  / 0
                          {{PI/2.,0,0},{-48.5,49.24,0},{4,-14.6585,-40.2765}}, // rotate Z + => counterclockwise  // 1
                          {{0,PI/2.,0},{0,-25.6757,28.4575},{0,-120,0}},      // rotate X + => up                // 2
                          {{0,-PI/2.,0},{6.3782,-24.2497,-19.2875},{13.88-6.3782,-89.75,5.3}}, // rotate X + => up      // 3
                          {{0,-PI/2.,0},{14,0,-11.5},{-14,-28,-5}},      // rotate Y + => counterclockwise      // 4
                          {{-PI/2.,0,0},{-6,-14,14},{-17,-58,14}},       // rotate X + => up      // 5
                          {{0,PI/2.,-PI/2.},{-5,38.3138,0},{10,0,0}},     // rotate Z + => close   // 6 right
                          {{0,-PI/2.,PI/2.},{5,38.3138,-8.5},{0,0,0}}};   // rotate Z + => open    // 7 left

color yellow    = color(255,255,0),
      yellow1   = color(255,255,200),
      red       = color(255,0,0),
      green     = color(0,255,0),
      green1    = color(204, 255, 51),
      blue      = color(0,0,255),
      turquoise = color(0,255,255),
      purple    = color(255,0,255),
      orange    = color(255,165,0);
      

color colorVec[] = {yellow,   
                    //yellow1,  
                    red,      
                    green,    
                    green1,   
                    blue,     
                    turquoise,
                    purple,   
                    orange};   
PImage tex;

PShape armI(int i){
  PShape res = loadShape(prefix+partVec[i]);
  res.setFill(colorVec[i]);
  //res.setTexture(tex);
  //res.rotateZ(positionVec[i][0][2]);
  res.rotateX(positionVec[i][0][0]);
  res.rotateY(positionVec[i][0][1]);
  res.rotate(positionVec[i][0][2], 0, 0, 1);
  res.translate(positionVec[i][1][0],
                positionVec[i][1][1],
                positionVec[i][1][2]);  
  return res;
}

float x0,y0;
PShape sVec[];


void setup(){
  fullScreen(P3D);
  //size(1900,1080,P3D);
  //tex = loadImage("Flat.png"); //("iguana.jpg"); //("colors_01.jpg");
  //textureMode(IMAGE);
  x0=width/2.;
  y0=height/2.;
  lights();
  sVec = new PShape[8];
  for (int i=0;i<partVec.length;i++){
    sVec[i] = armI(i);
  }
  frameRate(25);
}

void allEm(){
  for (int i=0;i<partVec.length;i++){
    translate(20*i,20*i);
    shape(sVec[i]);
  }
}

void lineIt(){
  pushMatrix();
  pushStyle();
  translate(x0,y0);
  stroke(red);   // X == RED
  line(-width/2.,0,0,
       width/2.,0,0);
  stroke(green);  // Y == GREEN
  line(0,-height/2.,0,
       0,height/2.,0);
  stroke(blue);   // Z ++ BLUE
  line(0,0,-height/2.,
       0,0,height/2.);
  popStyle();
  popMatrix();
}

float defaultAngleVec[] = {radians(0),       // 0 base no rotation
                       radians(0),      // 1 waist
                       radians(90),      // 2 shoulder
                       radians(0),      // 3 elbow
                       radians(0),      // 4 wrist roll
                       radians(0),      // 5 wirst elevation
                       radians(0),      // 6 left grip
                       radians(-0)};      // 7 right girp

float rotationVec[] = {radians(0),       // 0 base no rotation
                       radians(0),      // 1 waist
                       radians(90),      // 2 shoulder
                       radians(0),      // 3 elbow
                       radians(0),      // 4 wrist roll
                       radians(0),      // 5 wirst elevation
                       radians(0),      // 6 left grip
                       radians(-0)};      // 7 right girp
                       
float incVec[] = {radians(0),
                 radians(1),
                 radians(2),
                 radians(3),
                 radians(4),
                 radians(5),
                 radians(1),
                 -radians(1)};
void doInc(){
  for (int i=1; i < incVec.length;i++){
    rotationVec[i]+=incVec[i];
    if ((rotationVec[i] <  (i > 5 ? radians(-20)  : defaultAngleVec[i]-PI/2.)) || (rotationVec[i] > (i>5 ? radians(20) : defaultAngleVec[i]+PI/2.))){
      incVec[i] = -incVec[i];
    }
  }
}
                       
void draw(){
  camera(width/5., height/5., 0.3*(height/2.) / tan(PI/6), 
         width/2., height/2., height/4., 
         0, 0,-1); //1,0);
  background(0);
  lights();
  lineIt();
  pushStyle();
  fill(100);
  rect(0,0,width,height);
  popStyle();
  translate(x0,y0,0);
  rotateZ(rotationVec[0]);
  shape(sVec[0]);
  translate(-positionVec[0][2][0],
            -positionVec[0][2][1],
            -positionVec[0][2][2]);
  rotateZ(rotationVec[1]);            
  shape(sVec[1]);
  translate(-positionVec[1][2][0],
            -positionVec[1][2][1],
            -positionVec[1][2][2]);
  rotateX(rotationVec[2]);
  shape(sVec[2]);
  translate(-positionVec[2][2][0],
            -positionVec[2][2][1],
            -positionVec[2][2][2]);
  rotateX(rotationVec[3]);
  shape(sVec[3]);
  translate(-positionVec[3][2][0],
            -positionVec[3][2][1],
            -positionVec[3][2][2]);
  rotateY(rotationVec[4]);
  shape(sVec[4]);
  translate(-positionVec[4][2][0],
            -positionVec[4][2][1],
            -positionVec[4][2][2]);
  rotateX(rotationVec[5]);
  shape(sVec[5]);
  translate(-positionVec[5][2][0],
            -positionVec[5][2][1],
            -positionVec[5][2][2]);
  pushMatrix();         
  rotateZ(rotationVec[6]);
  shape(sVec[6]);
  popMatrix();
  translate(-positionVec[6][2][0],
            -positionVec[6][2][1],
            -positionVec[6][2][2]);
  rotateZ(rotationVec[7]);
  shape(sVec[7]);
  doInc();
}
  

void translateTriplet(float v[]){
  translate(v[0],v[1],v[2]);
}
