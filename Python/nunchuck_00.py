#!/usr/bin/python3

import tkinter as tk
from tkinter import messagebox
from os import getcwd
import serial
from time import sleep,time
from functools import reduce

versionString = 'simGui: ' + reduce(lambda a,b:a+'/'+b,((getcwd().split('/')[-2:])))


speedColor      = 'SeaGreen2'
speedTroughColor= 'SeaGreen3'
activeColor     = 'blue'
forwardColor    = 'green'
reverseColor    = 'cyan4'
entryColor      = 'white'
limitColor      = 'SeaGreen1'
limitActiveColor= 'cornsilk3'
limitErrorColor = 'red'
stopColor       = 'red'
pauseColor      = 'yellow'
listColor       = 'gray80'
doubleSNColor   = 'orange2'
saveColor       = 'cornsilk3'
quitColor       = 'cornsilk4'
countColor      = ['SeaGreen1',       # 0
                   'SeaGreen2',       # 1
                   'SeaGreen3',       # 2
                   'PaleGreen1',      # 3
                   'PaleGreen2',      # 4
                   'PaleGreen3',      # 5
                   'PaleGreen4',      # 6
                   'goldenrod1',      # 7
                   'goldenrod2',      # 8
                   'goldenrod3',      # 9
                   'goldenrod4',      # 10
                   'firebrick1',      # 11
                   'firebrick2',      # 12
                   'firebrick3',      # 13
                   'firebrick4',      # 14
                   'orange red']      # 15

class Nunchucker():
    valueNameVec = ['JoyX',
                    'JoyY',
                    'AccX',
                    'AccY',
                    'AccZ',
                    'ButZ',
                    'ButC']
    rangeVec = [[-127,128],[-127,128],
                [-1000,1000],[-1000,1000],[-1000,1000],
                [0,1],[0,1]]

    powerVec = [[-2*127/3,-127/3, 0, 128/3, 2*128/3],
                [-2*127/3,-127/3, 0, 128/3, 2*128/3],
                [-200,-100,0,100,200],[-200,-100,0,100,200],[-200,-100,0,100,200]]
                    
    def __init__(self,serialPort):
        self.serial = serial.Serial(serialPort, baudrate=9600)
        self.nameIndexDict = {}
        for i in range(len(Nunchucker.valueNameVec)):
            self.nameIndexDict[Nunchucker.valueNameVec[i]] = i
        

    def rangeIt(self,val,pVec):
        i = -3
        for vLim in pVec:
            i+=1
            if val <= vLim:
                break
        return i
    
    def getRangeVec(self,valVec):
        res  = []
        for val,pVec in zip(valVec,Nunchucker.powerVec):
            res.append(self.rangeIt(float(val),pVec))
        for val in valVec[-2:]:
            res.append(int(val))
        return res

    def getValVec(self):
        incoming = self.serial.readline()
        return self.getRangeVec(incoming.decode().strip().split(','))

class Gui(tk.Frame):
    handMM     = [0,21]
    wristMM    = [-150,150]
    rollMM     = [-180,180]
    elbowMM    = [-160,160]
    shoulderMM = [-180,180]
    waistMM    = [-180,180]
    dAlphaDt   = 1.0/20  # 0.05degrees/millissec
    
    def __init__(self,serialPort,master=None):
        tk.Frame.__init__(self, master)
        self.serialPort = serialPort
        self.serial = serial.Serial(serialPort,baudrate= 115200)
        self.master.title("Robo Tarn ")
        self.dataFileName = 'positions.txt'
        self.master.geometry('530x1000')                
        self.grid()
        self.commandVec = ['sv0000', 'sr0000', 'sp0000', 'sn0000','sd0000','sw0000']
                         #['SAVE',   'RESET',  'PAUSE',  'RUN',    READ-file, 'Write-file']
        self.currentSpeed = tk.IntVar()
        self.currentSpeed.set(180)
        self.currentHand = tk.IntVar()
        self.currentHand.set(0)
        self.currentWrist = tk.IntVar()
        self.currentWrist.set(0)
        self.currentRoll = tk.IntVar()
        self.currentRoll.set(0)
        self.currentElbow = tk.IntVar()
        self.currentElbow.set(0)
        self.currentShoulder = tk.IntVar()
        self.currentShoulder.set(0) #(90)
        self.currentWaist = tk.IntVar()
        self.currentWaist.set(0)
        self.servoPostionVarDict = {'ss':self.currentSpeed,
                                    's1':self.currentWaist,
                                    's2':self.currentShoulder,
                                    's3':self.currentElbow,
                                    's4':self.currentRoll,
                                    's5':self.currentWrist,
                                    's6':self.currentHand}
        #self.countString  = tk.StringVar()
        self.widgetDict = {}
                
        saveButtonArgDict = {  'widget': 'tk.Button',
                              'argDict' : {'master' : self.master,
                                           'name'   : 'saveButton',
                                           'text'   : 'SAVE',
                                           'bg'     : forwardColor,
                                           'activebackground' : activeColor,                       
                                           'command'          : lambda : self.actionCallBack(0)},
                              'gridDict' : {'row'    : 0,
                                            'column' : 0,
                                            'sticky' : 'NSEW'}} 

        resetButtonArgDict = {  'widget': 'tk.Button',
                                'argDict' : {'master' : self.master,
                                             'name'   : 'resetButton',
                                             'text'   :  'RESET',
                                             'bg'     : reverseColor,
                                             'activebackground' : activeColor,
                                             'command'          : lambda : self.actionCallBack(1)},
                                  'gridDict' : {'row'    : 0,
                                                'column' : 1,
                                                'sticky' : 'NSEW'}}

        pauseButtonArgDict = {'widget': 'tk.Button',
                              'argDict' : {'master' : self.master,
                                           'name'   : 'pauseButton',
                                           'text'   : 'PAUSE',
                                           'bg'     : activeColor,
                                           'activebackground' : activeColor,
                                           'command'          : lambda : self.actionCallBack(2)},
                             'gridDict' : {'row'    : 0,
                                           'column' : 2,
                                           'sticky' : 'NSEW'}}
        
        runButtonArgDict = {  'widget': 'tk.Button',
                              'argDict' : {'master'  : self.master,
                                           'name'    : 'runButton',
                                           'text'    : 'RUN',
                                           'bg'      : pauseColor,
                                           'activebackground' : activeColor,
                                           'command'          : lambda : self.actionCallBack(3)},
                              'gridDict' : {'row'    : 0,
                                            'column' : 3,
                                            'sticky' : 'NSEW'}}
        
        reconnectButtonArgDict = {  'widget': 'tk.Button',
                                    'argDict' : {'master'  : self.master,
                                                 'name'    : 'reconnectButton',
                                                 'text'    : 'RECONNECT',
                                                 'bg'      : forwardColor,
                                                 'activebackground' : activeColor,
                                                 'command' : lambda : self.actionCallBack(-1)},
                                    'gridDict' : {'row'    : 0,
                                                  'column' : 4,
                                                  'sticky' : 'NSEW'}}
        
        SpeedSliderDict   = { 'widget'  : 'tk.Scale',
                              'argDict' : {'master'       : self.master,
                                           'name'         : 'speedSlider',
                                           'bg'           : countColor[7], #speedColor,
                                           'troughcolor'  : countColor[8], #speedTroughColor,
                                           'label'        : 'Vitesse',
                                           'sliderrelief' : 'raised',
                                           'relief'       : 'sunken',
                                           'from_'        : 0,
                                           'to'           : 180,
                                           'orient'       : tk.HORIZONTAL,
                                           'length'       : 350,
                                           'showvalue'    : 1,
                                           'resolution'   : 1,
                                           'variable'     : self.currentSpeed},
                              'gridDict' : {'row'    : 1,
                                            'column' : 0,
                                            'columnspan' : 5,
                                            'sticky' : 'NSEW'},
                              'confFunc' : lambda s: self.configurePosition('ss')}
        
        HandSLiderDict   = {  'widget'  : 'tk.Scale',
                              'argDict' : {'master'       : self.master,
                                           'name'         : 'handSlider',
                                           'bg'           : countColor[0],
                                           'troughcolor'  : countColor[1],#speedTroughColor,
                                           'label'        : 'Main',
                                           'sliderrelief' : 'raised',
                                           'relief'       : 'sunken',
                                           'from_'        : 0,
                                           'to'           : 21,
                                           'orient'       : tk.HORIZONTAL,
                                           'length'       : 350,
                                           'showvalue'    : 1,
                                           'resolution'   : 1,
                                           'variable'     : self.currentHand},
                                           #'command'      : lambda s: self.configurePosition('s6')},
                              'gridDict' : {'row'    : 2,

                                            'columnspan' : 5,
                                            'sticky' : 'NSEW'},
                              'confFunc' : lambda s: self.configurePosition('s6')}

        WristSLiderDict  = {  'widget'  : 'tk.Scale',
                              'argDict' : {'master'       : self.master,
                                           'name'         : 'wristSlider',
                                           'bg'           : countColor[0],
                                           'troughcolor'  : countColor[1],#speedTroughColor,
                                           'label'        : 'Poignée',
                                           'sliderrelief' : 'raised',
                                           'relief'       : 'sunken',
                                           'from_'        : -150,
                                           'to'           : 150,
                                           'orient'       : tk.HORIZONTAL,
                                           'length'       : 350,
                                           'showvalue'    : 1,
                                           'resolution'   : 1,
                                           'variable'     : self.currentWrist},
                                           #'command'      : lambda s: self.configurePosition('s5')},
                              'gridDict' : {'row'    : 3,
                                            'column' : 0,
                                            'columnspan' : 5,
                                            'sticky' : 'NSEW'},
                              'confFunc' : lambda s: self.configurePosition('s5')}
        RollSLiderDict   = {  'widget'  : 'tk.Scale',
                              'argDict' : {'master'       : self.master,
                                           'name'         : 'rollSlider',
                                           'bg'           : countColor[0],
                                           'troughcolor'  : countColor[1], #speedTroughColor,
                                           'label'        : 'Rotation Poignée',
                                           'sliderrelief' : 'raised',
                                           'relief'       : 'sunken',
                                           'from_'        : -180,
                                           'to'           : 180,
                                           'orient'       : tk.HORIZONTAL,
                                           'length'       : 350,
                                           'showvalue'    : 1,
                                           'resolution'   : 1,
                                           'variable'     : self.currentRoll},
                                           #'command'      : lambda s: self.configurePosition('s4')},
                              'gridDict' : {'row'    : 4,
                                            'column' : 0,
                                            'columnspan' : 5,
                                            'sticky' : 'NSEW'},
                              'confFunc' : lambda s: self.configurePosition('s4')}
        ElbowSLiderDict  = {  'widget'  : 'tk.Scale',
                              'argDict' : {'master'       : self.master,
                                           'name'         : 'elbowSlider',
                                           'bg'           : countColor[0],
                                           'troughcolor'  : countColor[1], #speedTroughColor,
                                           'label'        : 'Coude',
                                           'sliderrelief' : 'raised',
                                           'relief'       : 'sunken',
                                           'from_'        : -160,
                                           'to'           : 160,
                                           'orient'       : tk.HORIZONTAL,
                                           'length'       : 350,
                                           'showvalue'    : 1,
                                           'resolution'   : 1,
                                           'variable'     : self.currentElbow},
                                           #'command'      : lambda s: self.configurePosition('s3')},
                              'gridDict' : {'row'    : 5,
                                            'column' : 0,
                                            'columnspan' : 5,
                                            'sticky' : 'NSEW'},
                              'confFunc' : lambda s: self.configurePosition('s3')}
        ShoulderSLiderDict  = {  'widget'  : 'tk.Scale',
                                 'argDict' : {'master'       : self.master,
                                              'name'         : 'shoulderSlider',
                                              'bg'           : countColor[0], #speedColor,
                                              'troughcolor'  : countColor[1], #speedTroughColor,
                                              'label'        : 'Epaule',
                                              'sliderrelief' : 'raised',
                                              'relief'       : 'sunken',
                                              'from_'        : -180,  #-90
                                              'to'           : 180,  # 270
                                              'orient'       : tk.HORIZONTAL,
                                              'length'       : 350,
                                              'showvalue'    : 1,
                                              'resolution'   : 1,
                                              'variable'     : self.currentShoulder},
                                              #'command'      : lambda s: self.configurePosition('s2')},
                              'gridDict' : {'row'    : 6,
                                            'column' : 0,
                                            'columnspan' : 5,
                                            'sticky' : 'NSEW'},
                              'confFunc' : lambda s: self.configurePosition('s2')}
        WaistSLiderDict  = {  'widget'  : 'tk.Scale',
                              'argDict' : {'master'       : self.master,
                                           'name'         : 'waistSlider',
                                           'bg'           : countColor[0], #speedColor,
                                           'troughcolor'  : countColor[1], #speedTroughColor,
                                           'label'        : 'Talle',
                                           'sliderrelief' : 'raised',
                                           'relief'       : 'sunken',
                                           'from_'        : -180,
                                           'to'           : 180,
                                           'orient'       : tk.HORIZONTAL,
                                           'length'       : 350,
                                           'showvalue'    : 1,
                                           'resolution'   : 1,
                                           'variable'     : self.currentWaist},
                                           #'command'      : lambda s: self.configurePosition('s1')},
                              'gridDict' : {'row'    : 7,
                                            'column' : 0,
                                            'columnspan' : 5,
                                            'sticky' : 'NSEW'},
                              'confFunc' : lambda s: self.configurePosition('s1')}
        saveFrameArgDict = { 'widget': 'tk.Frame',
                             'argDict' : {'master'  : self.master,
                                          'name'    : 'saveFrame',
                                          'bd'      : 1,
                                          'relief'  : 'sunken'},
                             'gridDict' : {'row'        : 8,
                                           'column'     : 0,
                                           'columnspan' : 5,
                                           'sticky'     : 'NSEW'}}
        readButtonArgDict = {  'widget': 'tk.Button',
                               'argDict' : {'master'  : lambda : self.widgetDict['saveFrame'],
                                            'name'    : 'readButton',
                                            'text'    : 'Read Data File',
                                            'bg'      : reverseColor,
                                            'activebackground' : activeColor,
                                            'command' : lambda : self.actionCallBack(4)},
                               'gridDict' : {'row'    : 0,
                                             'column' : 0,
                                             #'columnspan' : 2,
                                             'sticky' : 'NSEW'}}
        writeButtonArgDict = {  'widget': 'tk.Button',
                                'argDict' : {'master'  : lambda : self.widgetDict['saveFrame'],
                                             'name'    : 'writeButton',
                                             'text'    : 'Write Data File',
                                             'bg'      : pauseColor,
                                             'activebackground' : activeColor,
                                             'command' : lambda : self.actionCallBack(5)},
                                'gridDict' : {'row'    : 0,
                                              'column' : 1,
                                              #'columnspan' : 2,
                                              'sticky' : 'NSEW'}}

        quitButtonArgDict = {'widget': 'tk.Button',
                             'argDict' : {'master' : lambda : self.widgetDict['saveFrame'],#self.master,
                                          'name'   : 'quitButton',
                                          'text'   : 'Quit',
                                          'bg'     :  quitColor,
                                          'activebackground' : activeColor,
                                          'command'          : self.doQuit},
                             'gridDict' : {'row'    : 1,
                                           'column' : 0,
                                           'columnspan' : 2,
                                           'sticky' : 'NSEW'}}
        widgetSpecs = [saveButtonArgDict,
                       resetButtonArgDict,
                       pauseButtonArgDict,
                       runButtonArgDict,
                       reconnectButtonArgDict,
                       SpeedSliderDict,
                       HandSLiderDict,
                       RollSLiderDict,                       
                       WristSLiderDict,
                       ElbowSLiderDict,
                       ShoulderSLiderDict,
                       WaistSLiderDict,
                       saveFrameArgDict,
                       readButtonArgDict,
                       writeButtonArgDict,
                       quitButtonArgDict]
       
        self.createWidgets(widgetSpecs)
        self.doBindings()
        self.finalizeFrames()
        self.resetPositions()
        self.nunchuck = Nunchucker('/dev/ttyACM0')
        self.nunchuckVec = []
        self.lastUpdateTimeMillis = time()*1000
        self.updateFromNunchuck()
        self.updateAccels()

    def updateVal(self, nunchuckVal,deltaTMillis,variable,mIN,mAX):
        curVal = variable.get()
        newVal = min(mAX,max(mIN,curVal + Gui.dAlphaDt*nunchuckVal*deltaTMillis))
        variable.set(newVal)

    def updateAccels(self):
        if all(self.nunchuckVec[-2:]):
            print('bang')
        else:
            setPairs = [[self.currentHand, Gui.handMM],
                        [self.currentWrist, Gui.wristMM],
                        [self.currentWaist, Gui.waistMM] if not self.nunchuckVec[5] else [self.currentRoll, Gui.rollMM],
                        [self.currentShoulder, Gui.shoulderMM] if not self.nunchuckVec[6] else [self.currentElbow, Gui.elbowMM]]
            now = time()*1000
            deltaMillis = now -self.lastUpdateTimeMillis
            self.lastUpdateTimeMillis = now        

            for i in range(4):
                self.updateVal(self.nunchuckVec[i],deltaMillis,setPairs[i][0],*setPairs[i][1])
        self.after(100, self.updateAccels)
              
        
    def updateFromNunchuck(self):
        self.nunchuckVec = self.nunchuck.getValVec()
        #self.processNunchuckVec()
        #print(self.nunchuckVec)
        self.after(20, self.updateFromNunchuck)
        
        
    def  createWidgets(self,widgetSpecs):
        for dict in widgetSpecs:
            try:
                dict['argDict']['master'] = dict['argDict']['master']()
            except:
                pass
            self.widgetDict[dict['argDict']['name']] = eval(dict['widget'])( **dict['argDict'])
            self.widgetDict[dict['argDict']['name']].grid(**dict['gridDict'])
            try:
                dict['confFunc']()
            except:
                pass
                
    def doBindings(self):
        self.master.bind('<Alt-v>',     lambda *Args : self.showVersion() )
        self.master.bind('<Alt-V>',     lambda *Args : self.showVersion() )
        self.master.bind('<Control-V>',     lambda *Args : self.showVersion() )
        self.master.bind('<Control-v>',     lambda *Args : self.showVersion() )
        self.master.bind('<Alt-x>',     lambda *args : self.doQuit() )
        self.master.bind('<Control-q>', lambda *args : self.doQuit() )
        self.master.bind('<Control-z>', lambda *args : self.doZero() )
        self.widgetDict['speedSlider'].bind('<ButtonRelease-1>',    lambda *args :  self.configurePosition('ss'))
        self.widgetDict['handSlider'].bind('<ButtonRelease-1>',     lambda *args :  self.configurePosition('s6'))
        self.widgetDict['wristSlider'].bind('<ButtonRelease-1>',    lambda *args :  self.configurePosition('s5'))
        self.widgetDict['rollSlider'].bind('<ButtonRelease-1>',     lambda *args :  self.configurePosition('s4'))
        self.widgetDict['elbowSlider'].bind('<ButtonRelease-1>',    lambda *args :  self.configurePosition('s3'))
        self.widgetDict['shoulderSlider'].bind('<ButtonRelease-1>', lambda *args :  self.configurePosition('s2'))
        self.widgetDict['waistSlider'].bind('<ButtonRelease-1>',    lambda *args :  self.configurePosition('s1'))        
        
    def finalizeFrames(self):
        colWeights = [1 for x in range(5)]
        rowWeights = [1 for x in range(9)]
        nbCols = len(colWeights)
        nbRows = len(rowWeights)

        self.widgetDict['saveFrame'].rowconfigure(0, weight=1)
        self.widgetDict['saveFrame'].rowconfigure(1, weight=1)
        self.widgetDict['saveFrame'].columnconfigure(0,weight=1)
        self.widgetDict['saveFrame'].columnconfigure(1,weight=1)

        for x in range(nbCols):
            self.master.columnconfigure( x, weight=colWeights[x])
        
        for y in range(nbRows):
            self.master.rowconfigure(y, weight=rowWeights[y])

    ##################### VERSION ####################################
            
    def showVersion(self,*args):
        messagebox.showinfo('System Version: ', versionString)

    ################## Action #####

    def actionCallBack(self,commandID):
        #print(commandID,type(commandID))
        if commandID == -1 :
            self.reconnect()
            return
        self.sendCommand(self.commandVec[commandID])

    ######################## Sliders ####################################

    def configurePosition(self,servoName,pos=None):
        #print(servoName)
        #print(type(self.servoPostionVarDict[servoName].get()))
        self.sendCommand(servoName + '{:04d}'.format(pos if pos else self.servoPostionVarDict[servoName].get()))
        #sleep(1)

    ############################# QUIT ###############################
    def doQuit(self):
        self.destroy()
    def destroy(self):
        """messagebox.showinfo('Patience', 'Remise du bras en positon initiale...')
        for name in ['s'+str(x) for x in range(1,7)]:
            self.configurePosition(name,90)
            sleep(1)
        """
        self.quit()

    ############################# RECONNECT ###############################

    def reconnect(self):
        print('reconnecting..')
        self.serial.close()
        sleep(1);
        self.serial = serial.Serial(self.serialPort,baudrate= 115200)
        #sleep(1)
        self.resetPositions()

    def resetPositions(self):
        #print(list(self.servoPostionVarDict.keys()))
        for servoName in self.servoPostionVarDict.keys():
            self.configurePosition(servoName)
        
   
    ############################# QUIT ###############################
    def doZero(self):
        self.currentSpeed.set(180)
        self.currentHand.set(0)
        self.currentWrist.set(0)
        self.currentRoll.set(0)
        self.currentElbow.set(0)
        self.currentShoulder.set(0) #(90)
        self.currentWaist.set(0)
        for p in ['ss','s6','s5','s4','s3','s2','s1']:
            self.configurePosition(p)

    ############################# SEND COMMAND ###############################

    def sendCommand(self, command):
        print('Sending : ', command);
        #self.serial.write(str.encode(command))


def runIt():
    #app = Gui('/dev/ttyACM0')
    app = Gui('/home/bob/ttyABC')
    #app = Nunchucker('/dev/ttyACM0')
    app.mainloop()


if __name__ == '__main__':
    runIt()
