#!/usr/bin/python3

import tkinter as tk
from tkinter import messagebox
from os import getcwd
import serial
from time import sleep

versionString = 'Gui: '# + getcwd().split('/')[-2]

defaultStartSpeedPct  = 30

badLabelString = 'BAD_LABEL'

speedColor      = 'SeaGreen2'
speedTroughColor= 'SeaGreen3'
activeColor     = 'blue'
forwardColor    = 'green'
reverseColor    = 'cyan4'
entryColor      = 'white'
limitColor      = 'SeaGreen1'
limitActiveColor= 'cornsilk3'
limitErrorColor = 'red'
stopColor       = 'red'
pauseColor      = 'yellow'
listColor       = 'gray80'
badTagColor     = 'red'
doubleSNColor   = 'orange2'
saveColor       = 'cornsilk3'
quitColor       = 'cornsilk4'
countColor      = ['SeaGreen1',       # 0
                   'SeaGreen2',       # 1
                   'SeaGreen3',       # 2
                   'PaleGreen1',      # 3
                   'PaleGreen2',      # 4
                   'PaleGreen3',      # 5
                   'PaleGreen4',      # 6
                   'goldenrod1',      # 7
                   'goldenrod2',      # 8
                   'goldenrod3',      # 9
                   'goldenrod4',      # 10
                   'firebrick1',      # 11
                   'firebrick2',      # 12
                   'firebrick3',      # 13
                   'firebrick4',      # 14
                   'orange red']      # 15

def getCountColor(nbGood=0,nbBad=0):
    if nbBad == 0:
        return countColor[0]
    else:
        failurePct = min(len(countColor)-1,round(100*nbBad/(nbGood+nbBad)))
        return countColor[failurePct]


### simulation variables
listBoxPollDelay = 100 # milliseconds
maxSNLength  = 30
maxQWait     = 5 # seconds

class Gui(tk.Frame):
    commandDict = {'l' : '0',    # followed by argument as a string!
                   'f' : '1',    # forward
                   'r' : '2',    # revverse
                   's' : '3',    # stop
                   'p' : '4',    # pause
                   'v' :  None,  # not implemented
                   'h' :  None,  # not implemented
                   'g' : '6'}    # followed by arg as string 
    def __init__(self,serialPort,master=None):
        tk.Frame.__init__(self, master)
        self.serial = serial.Serial(serialPort,baudrate= 115200)
        self.master.title("Robo Tarn ")
        self.master.geometry('1000x800')                
        self.grid()
        self.commandVec = ['SAVE', 'RESET', 'PAUSE','RUN']
        self.currentSpeed = tk.IntVar()
        self.currentSpeed.set(20)
        self.currentHand = tk.IntVar()
        self.currentHand.set(90)
        self.currentWrist = tk.IntVar()
        self.currentWrist.set(90)
        self.currentRoll = tk.IntVar()
        self.currentRoll.set(90)
        self.currentElbow = tk.IntVar()
        self.currentElbow.set(90)
        self.currentShoulder = tk.IntVar()
        self.currentShoulder.set(90)
        self.currentWaist = tk.IntVar()
        self.currentWaist.set(90)
        self.servoPostionVarDict = {'ss':self.currentSpeed,
                                    's1':self.currentWaist,
                                    's2':self.currentShoulder,
                                    's3':self.currentElbow,
                                    's4':self.currentRoll,
                                    's5':self.currentWrist,
                                    's6':self.currentHand}
        #self.countString  = tk.StringVar()
        self.widgetDict = {}
                
        saveButtonArgDict = {  'widget': 'tk.Button',
                              'argDict' : {'master' : self.master,
                                           'name'   : 'saveButton',
                                           'text'   : 'SAVE',
                                           'bg'     : forwardColor,
                                           'activebackground' : activeColor,                       
                                           'command'          : lambda : self.actionCallBack(0)},
                              'gridDict' : {'row'    : 0,
                                            'column' : 0,
                                            'sticky' : 'NSEW'}} 

        resetButtonArgDict = {  'widget': 'tk.Button',
                                'argDict' : {'master' : self.master,
                                             'name'   : 'resetButton',
                                             'text'   :  'RESET',
                                             'bg'     : reverseColor,
                                             'activebackground' : activeColor,
                                             'command'          : lambda : self.actionCallBack(1)},
                                  'gridDict' : {'row'    : 0,
                                                'column' : 1,
                                                'sticky' : 'NSEW'}}

        pauseButtonArgDict = {'widget': 'tk.Button',
                              'argDict' : {'master' : self.master,
                                           'name'   : 'pauseButton',
                                           'text'   : 'PAUSE',
                                           'bg'     : activeColor,
                                           'activebackground' : activeColor,
                                           'command'          : lambda : self.actionCallBack(2)},
                             'gridDict' : {'row'    : 0,
                                           'column' : 2,
                                           'sticky' : 'NSEW'}}
        
        runButtonArgDict = {  'widget': 'tk.Button',
                              'argDict' : {'master'  : self.master,
                                           'name'    : 'runButton',
                                           'text'    : 'RUN',
                                           'bg'      : pauseColor,
                                           'activebackground' : activeColor,
                                           'command'          : lambda : self.actionCallBack(3)},
                              'gridDict' : {'row'    : 0,
                                            'column' : 3,
                                            'sticky' : 'NSEW'}}
        
        SpeedSliderDict   = { 'widget'  : 'tk.Scale',
                              'argDict' : {'master'       : self.master,
                                           'name'         : 'speedSlider',
                                           'bg'           : countColor[7], #speedColor,
                                           'troughcolor'  : countColor[8], #speedTroughColor,
                                           'label'        : 'Vitesse',
                                           'sliderrelief' : 'raised',
                                           'relief'       : 'sunken',
                                           'from_'        : 0,
                                           'to'           : 180,
                                           'orient'       : tk.HORIZONTAL,
                                           'length'       : 350,
                                           'showvalue'    : 1,
                                           'resolution'   : 1,
                                           'variable'     : self.currentSpeed},
                              'gridDict' : {'row'    : 1,
                                            'column' : 0,
                                            'columnspan' : 4,
                                            'sticky' : 'NSEW'},
                              'confFunc' : lambda s: self.configurePosition('ss')}
        
        HandSLiderDict   = {  'widget'  : 'tk.Scale',
                              'argDict' : {'master'       : self.master,
                                           'name'         : 'handSlider',
                                           'bg'           : countColor[0],
                                           'troughcolor'  : countColor[1],#speedTroughColor,
                                           'label'        : 'Main',
                                           'sliderrelief' : 'raised',
                                           'relief'       : 'sunken',
                                           'from_'        : 0,
                                           'to'           : 180,
                                           'orient'       : tk.HORIZONTAL,
                                           'length'       : 350,
                                           'showvalue'    : 1,
                                           'resolution'   : 1,
                                           'variable'     : self.currentHand},
                                           #'command'      : lambda s: self.configurePosition('s6')},
                              'gridDict' : {'row'    : 2,
                                            'column' : 0,
                                            'columnspan' : 4,
                                            'sticky' : 'NSEW'},
                              'confFunc' : lambda s: self.configurePosition('s6')}

        WristSLiderDict  = {  'widget'  : 'tk.Scale',
                              'argDict' : {'master'       : self.master,
                                           'name'         : 'wristSlider',
                                           'bg'           : countColor[0],
                                           'troughcolor'  : countColor[1],#speedTroughColor,
                                           'label'        : 'Poignée',
                                           'sliderrelief' : 'raised',
                                           'relief'       : 'sunken',
                                           'from_'        : 0,
                                           'to'           : 180,
                                           'orient'       : tk.HORIZONTAL,
                                           'length'       : 350,
                                           'showvalue'    : 1,
                                           'resolution'   : 1,
                                           'variable'     : self.currentWrist},
                                           #'command'      : lambda s: self.configurePosition('s5')},
                              'gridDict' : {'row'    : 3,
                                            'column' : 0,
                                            'columnspan' : 4,
                                            'sticky' : 'NSEW'},
                              'confFunc' : lambda s: self.configurePosition('s5')}
        RollSLiderDict   = {  'widget'  : 'tk.Scale',
                              'argDict' : {'master'       : self.master,
                                           'name'         : 'rollSlider',
                                           'bg'           : countColor[0],
                                           'troughcolor'  : countColor[1], #speedTroughColor,
                                           'label'        : 'Rotation Poignée',
                                           'sliderrelief' : 'raised',
                                           'relief'       : 'sunken',
                                           'from_'        : 0,
                                           'to'           : 180,
                                           'orient'       : tk.HORIZONTAL,
                                           'length'       : 350,
                                           'showvalue'    : 1,
                                           'resolution'   : 1,
                                           'variable'     : self.currentRoll},
                                           #'command'      : lambda s: self.configurePosition('s4')},
                              'gridDict' : {'row'    : 4,
                                            'column' : 0,
                                            'columnspan' : 4,
                                            'sticky' : 'NSEW'},
                              'confFunc' : lambda s: self.configurePosition('s4')}
        ElbowSLiderDict  = {  'widget'  : 'tk.Scale',
                              'argDict' : {'master'       : self.master,
                                           'name'         : 'elbowSlider',
                                           'bg'           : countColor[0],
                                           'troughcolor'  : countColor[1], #speedTroughColor,
                                           'label'        : 'Coude',
                                           'sliderrelief' : 'raised',
                                           'relief'       : 'sunken',
                                           'from_'        : 0,
                                           'to'           : 180,
                                           'orient'       : tk.HORIZONTAL,
                                           'length'       : 350,
                                           'showvalue'    : 1,
                                           'resolution'   : 1,
                                           'variable'     : self.currentElbow},
                                           #'command'      : lambda s: self.configurePosition('s3')},
                              'gridDict' : {'row'    : 5,
                                            'column' : 0,
                                            'columnspan' : 4,
                                            'sticky' : 'NSEW'},
                              'confFunc' : lambda s: self.configurePosition('s3')}
        ShoulderSLiderDict  = {  'widget'  : 'tk.Scale',
                                 'argDict' : {'master'       : self.master,
                                              'name'         : 'shoulderSlider',
                                              'bg'           : countColor[0], #speedColor,
                                              'troughcolor'  : countColor[1], #speedTroughColor,
                                              'label'        : 'Epaule',
                                              'sliderrelief' : 'raised',
                                              'relief'       : 'sunken',
                                              'from_'        : 0,
                                              'to'           : 180,
                                              'orient'       : tk.HORIZONTAL,
                                              'length'       : 350,
                                              'showvalue'    : 1,
                                              'resolution'   : 1,
                                              'variable'     : self.currentShoulder},
                                              #'command'      : lambda s: self.configurePosition('s2')},
                              'gridDict' : {'row'    : 6,
                                            'column' : 0,
                                            'columnspan' : 4,
                                            'sticky' : 'NSEW'},
                              'confFunc' : lambda s: self.configurePosition('s2')}
        WaistSLiderDict  = {  'widget'  : 'tk.Scale',
                              'argDict' : {'master'       : self.master,
                                           'name'         : 'waistSlider',
                                           'bg'           : countColor[0], #speedColor,
                                           'troughcolor'  : countColor[1], #speedTroughColor,
                                           'label'        : 'Talle',
                                           'sliderrelief' : 'raised',
                                           'relief'       : 'sunken',
                                           'from_'        : 0,
                                           'to'           : 180,
                                           'orient'       : tk.HORIZONTAL,
                                           'length'       : 350,
                                           'showvalue'    : 1,
                                           'resolution'   : 1,
                                           'variable'     : self.currentWaist},
                                           #'command'      : lambda s: self.configurePosition('s1')},
                              'gridDict' : {'row'    : 7,
                                            'column' : 0,
                                            'columnspan' : 4,
                                            'sticky' : 'NSEW'},
                              'confFunc' : lambda s: self.configurePosition('s1')}
        
        quitButtonArgDict = {'widget': 'tk.Button',
                             'argDict' : {'master' : self.master,
                                          'name'   : 'quitButton',
                                          'text'   : 'Quit',
                                          'bg'     :  quitColor,
                                          'activebackground' : activeColor,
                                          'command'          : self.doQuit},
                             'gridDict' : {'row'    : 8,
                                           'column' : 0,
                                           'columnspan' : 4,
                                           'sticky' : 'NSEW'}}
        widgetSpecs = [saveButtonArgDict,
                       resetButtonArgDict,
                       pauseButtonArgDict,
                       runButtonArgDict,
                       SpeedSliderDict,
                       HandSLiderDict,
                       RollSLiderDict,                       
                       WristSLiderDict,
                       ElbowSLiderDict,
                       ShoulderSLiderDict,
                       WaistSLiderDict,
                       quitButtonArgDict]
       
        self.createWidgets(widgetSpecs)
        self.doBindings()
        self.finalizeFrames()
        
    def  createWidgets(self,widgetSpecs):
        for dict in widgetSpecs:
            try:
                dict['argDict']['master'] = dict['argDict']['master']()
            except:
                pass
            self.widgetDict[dict['argDict']['name']] = eval(dict['widget'])( **dict['argDict'])
            self.widgetDict[dict['argDict']['name']].grid(**dict['gridDict'])
            try:
                dict['confFunc']()
            except:
                pass
                
    def doBindings(self):
        self.master.bind('<Alt-v>',     lambda *Args : self.showVersion() )
        self.master.bind('<Alt-V>',     lambda *Args : self.showVersion() )
        self.master.bind('<Control-V>',     lambda *Args : self.showVersion() )
        self.master.bind('<Control-v>',     lambda *Args : self.showVersion() )
        self.master.bind('<Alt-x>',     lambda *args : self.doQuit() )
        self.master.bind('<Control-q>', lambda *args : self.doQuit() )
        self.widgetDict['speedSlider'].bind('<ButtonRelease-1>',    lambda *args :  self.configurePosition('ss'))
        self.widgetDict['handSlider'].bind('<ButtonRelease-1>',     lambda *args :  self.configurePosition('s6'))
        self.widgetDict['wristSlider'].bind('<ButtonRelease-1>',    lambda *args :  self.configurePosition('s5'))
        self.widgetDict['rollSlider'].bind('<ButtonRelease-1>',     lambda *args :  self.configurePosition('s4'))
        self.widgetDict['elbowSlider'].bind('<ButtonRelease-1>',    lambda *args :  self.configurePosition('s3'))
        self.widgetDict['shoulderSlider'].bind('<ButtonRelease-1>', lambda *args :  self.configurePosition('s2'))
        self.widgetDict['waistSlider'].bind('<ButtonRelease-1>',    lambda *args :  self.configurePosition('s1'))        
        
    def finalizeFrames(self):
        colWeights = [1 for x in range(4)]
        rowWeights = [1 for x in range(9)]
        nbCols = len(colWeights)
        nbRows = len(rowWeights)

        #self.widgetDict['saveFrame'].rowconfigure(0, weight=1)
        #self.widgetDict['saveFrame'].columnconfigure(0,weight=1)

        for x in range(nbCols):
            self.master.columnconfigure( x, weight=colWeights[x])
        
        for y in range(nbRows):
            self.master.rowconfigure(y, weight=rowWeights[y])

    ##################### VERSION ####################################
            
    def showVersion(self,*args):
        messagebox.showinfo('System Version: ', versionString)

    ################## Action #####

    def actionCallBack(self,commandID):
        if self.commandVec[commandID] == 'RUN':
            self.configurePosition('ss')
        self.sendCommand(self.commandVec[commandID])

    ######################## Sliders ####################################

    def configurePosition(self,servoName,pos=None):
        #self.servoPostionVarDict[servoName].set(pos)
        self.sendCommand(servoName + str(pos if pos else self.servoPostionVarDict[servoName].get()))
        sleep(1)

    ############################# QUIT ###############################
    def doQuit(self):
        self.destroy()
    def destroy(self):
        """messagebox.showinfo('Patience', 'Remise du bras en positon initiale...')
        for name in ['s'+str(x) for x in range(1,7)]:
            self.configurePosition(name,90)
            sleep(1)
        """
        self.quit()
        
        
    ############################# SEND COMMAND ###############################

    def sendCommand(self, command):
        print('Sending : ', command);
        self.serial.write(str.encode(command))


def runIt():
    app = Gui('/dev/ttyACM0')
    app.mainloop()


if __name__ == '__main__':
    runIt()
