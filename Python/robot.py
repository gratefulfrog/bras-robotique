#!/usr/bin/python3

from time import sleep
import serial
import sys
from gui import Gui 

"""
def showHelp():
        print('Usage: $ ./serialServer.py <DUE_port TagReader_port nbLabels testPauseTimeInSeconds>  default to /dev/ttyACM0 /dev/ttyACM1 100 10'  )
        print('                           if a pauseTime is given then test mode is activated')
        print('examples;')
        print('Usage: $ ./serialServer.py                                  # uses default ports, test pause time, and number of labels')
        print('Usage: $ ./serialServer.py /dev/ttyACM7 /dev/ttyACM9        # use these DUE on ttyACM7 end tag reader on ttyACM9')
        print('Usage: $ ./serialServer.py /dev/ttyACM7 /dev/ttyACM9   100  # use these ports and a 100 labels not a test')
        print('Usage: $ ./serialServer.py /dev/ttyACM7 /dev/ttyACM9   1000 100  # use these ports, 1000 labels and 20 second pause, this implies that it is a test!')
        print('Usage: $ ./serialServer.py -h                               # print this help message')
"""

def startup(arduinoPort):
    gui        = Gui(arduinoPort)
    gui.mainloop()


if __name__ == '__main__':
    ap = None
    if any(['-h' in sys.argv, '--h' in sys.argv, '--help' in sys.argv]):
        showHelp()
        sys.exit(0)

    if len(sys.argv) == 2:
        ap = sys.argv[1]
        
    else:    
        import findACM as facm
        ap = facm.getArduinoPort()
        if not ap:
            print('Arduino not found...')
    
    if ap:
        print('Arduino port  : ', ap)
        print ('Checking Serial Ports for availability..')
        waitingA = True
        count = 0
        while waitingA:
            try:
                sArd = serial.Serial(ap)
                sArd.close()
                waitingA = False
            except Exception as e:
                print(e)
                if waitingA:
                    print(count, 'Waiting on Arduino port...')
                    count +=1
            sleep(1)

        print('Starting up...')
        startup(ap)
    else:
        print('Port not found!')
        sys.exit(0)
           
