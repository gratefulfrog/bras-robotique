#!/usr/bin/python3

import subprocess

def getRaw():
    res = subprocess.run(['./ttyScan.bash'],stdout=subprocess.PIPE)
    lis = res.stdout.decode('utf-8').split('\n')
    lol = []
    for l in lis:
        lol.append(l.split(' - '))
        
    return lol

def findS(s,lol):
    res = []
    for pair in lol:
        try:
            if s in pair[1]:
                res.append(pair[0])
            else:
                pass
        except:
            pass
    return res

arduinoNameLis   = ['Arduino',
                    'www.arduino.cc']  

def findPort(pLis):
    for p in pLis:
        if p:
            return p[0]

def getArduinoPort():
    pLis = [findS(s,getRaw())  for s in arduinoNameLis]
    return findPort(pLis)
"""
def getTagReaderPort():
    pLis = [findS(s,getRaw())  for s in tagReaderNameLis]
    return findPort(pLis)
"""
if __name__ == '__main__':
    print(getArduinoPort())
    print('if not found, this is returned: ', findPort([]))

